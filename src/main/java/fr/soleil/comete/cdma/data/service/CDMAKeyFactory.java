/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.cdma.data.service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.cdma.Factory;
import org.cdma.IFactory;
import org.cdma.dictionary.filter.IFilter;

import fr.soleil.comete.cdma.data.commands.CommandContainer;
import fr.soleil.comete.cdma.data.commands.CommandData;
import fr.soleil.comete.cdma.data.commands.CommandURI;
import fr.soleil.comete.cdma.data.factories.SourceFactory;
import fr.soleil.comete.cdma.data.keys.CDMAKey;
import fr.soleil.comete.cdma.data.keys.KeyProperties;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Create keys for a CDMA data source, those keys will be used to determine a proper URI.
 * 
 * @author rodriguez
 * 
 */
public final class CDMAKeyFactory implements KeyProperties {

    // Members
    private BrowsingMode mode;
    private String view;
    private String pluginID;

    public CDMAKeyFactory() {
        this.mode = BrowsingMode.PHYSICAL;
        this.view = ObjectUtils.EMPTY_STRING;
        this.setPluginRestriction(null);
    }

    public CDMAKeyFactory(String view) {
        this.mode = BrowsingMode.LOGICAL;
        this.view = view == null ? ObjectUtils.EMPTY_STRING : view;
        this.setPluginRestriction(null);
    }

    // ---------------------------------------------------------
    // Factory management
    // ---------------------------------------------------------
    /**
     * Set the given browsing mode as the current
     * 
     * @param mode whether Logical or Physical
     * @note by default the Physical mode is used
     */
    public void enableMode(BrowsingMode mode) {
        this.mode = mode;
    }

    /**
     * Returns the currently active browsing mode.
     */
    public BrowsingMode getMode() {
        return this.mode;
    }

    /**
     * Set the current view when the Logical browsing mode is activated.
     */
    public void setLogicalView(String view) {
        this.view = view;
    }

    /**
     * Returns the currently used logical view.
     */
    public String getLogicalView() {
        return view;
    }

    /**
     * Returns all available views for the logical browsing mode
     */
    public List<String> getAvailableViews() {
        return Factory.getAvailableViews();
    }

    /**
     * Returns all available views for the logical browsing mode
     */
    static public List<String> getAvailablePlugins() {
        List<IFactory> factories = Factory.getFactories();
        List<String> result = new ArrayList<String>();
        for (IFactory factory : factories) {
            result.add(factory.getName());
        }
        return result;
    }

    /**
     * Tells if a named plugin can enable the Logical mode.
     * 
     * @param plugin name {@link CDMAKeyFactory.getAvailablePlugins()}
     * @return true if the Logical mode can be used with that plugin
     */
    static public boolean isLogicalModeAvailable(String plugin) {
        boolean result = false;
        if (plugin != null && !plugin.isEmpty()) {
            IFactory factory = Factory.getFactory(plugin);
            if (factory != null) {
                result = factory.isLogicalModeAvailable();
            }
        }

        return result;
    }

    /**
     * Returns true when a dataset as already been opened for the given URI.
     * The browsing mode isn't considered.
     */
    static public boolean isOpenedDataset(URI uri) {
        return SourceFactory.isOpenedDataset(uri);
    }

    /**
     * Create a key to list all attribute names on the targeted node
     * 
     * @param uri of the CDMA dataset to be opened
     * @param path of the CDMA container to be opened
     * 
     * @return CDMA key that will return a list of string
     */
    public CDMAKey generateKeyListAvailablePlugins() {
        CDMAKey key;
        try {
            key = new CDMAKey(new URI(ObjectUtils.EMPTY_STRING), CommandURI.ListAvailablePlugins, new String[] {});
            key.registerProperty(NAV_MODE, this.mode);
            key.registerProperty(NAV_VIEW, this.view);
            key.setRank(1);
        } catch (URISyntaxException e) {
            key = null;
        }

        return key;
    }

    /**
     * Return the plugin's name that all created keys will be restricted to use.
     * 
     * @return the plugin's name
     * @note if null all plugins can be used
     */
    public String getPluginRestriction() {
        return pluginID;
    }

    /**
     * Set the plugin's name that all created keys will be restricted to use.
     * 
     * @param name the plugin's name
     * @note if null all plugins can be used
     */
    public void setPluginRestriction(String name) {
        this.pluginID = name;
    }

    // ---------------------------------------------------------
    // Containers management methods
    // ---------------------------------------------------------
    /**
     * Create a key to list all attribute names on the targeted node
     * 
     * @param uri of the CDMA dataset to be opened
     * @param path of the CDMA container to be opened
     * 
     * @return CDMA key that will return a list of string
     */
    public CDMAKey generateKeyAttributeList(URI uri, String... path) {
        CDMAKey key = new CDMAKey(uri, CommandContainer.AttributeList, path);
        key.registerProperty(NAV_MODE, this.mode);
        key.registerProperty(NAV_VIEW, this.view);
        key.setRank(1);
        if (pluginID != null) {
            key.setPlugin(pluginID);
        }
        return key;
    }

    /**
     * Create a key to list all group names on the targeted group
     * 
     * @param uri of the CDMA dataset to be opened
     * @param path of the CDMA group to be opened
     * 
     * @return CDMA key that will return a list of string
     */
    public CDMAKey generateKeyGroupList(URI uri, String... path) {
        CDMAKey key = new CDMAKey(uri, CommandContainer.GroupList, path);
        key.registerProperty(NAV_MODE, this.mode);
        key.registerProperty(NAV_VIEW, this.view);
        key.setRank(1);
        if (pluginID != null) {
            key.setPlugin(pluginID);
        }
        return key;
    }

    /**
     * Create a key to list all dimension names on the targeted group
     * 
     * @param uri of the CDMA dataset to be opened
     * @param path of the CDMA group to be opened
     * 
     * @return CDMA key that will return a list of string
     */
    public CDMAKey generateKeyDimensionList(URI uri, String... path) {
        CDMAKey key = new CDMAKey(uri, CommandContainer.DimensionList, path);
        key.registerProperty(NAV_MODE, this.mode);
        key.registerProperty(NAV_VIEW, this.view);
        key.setRank(1);
        if (pluginID != null) {
            key.setPlugin(pluginID);
        }
        return key;
    }

    /**
     * Create a key to get the dimension's values of the name value in the given path
     * 
     * @param uri of the CDMA dataset to be opened
     * @param path of the CDMA group to be opened
     * @param name of the dimension to get value
     * 
     * @return CDMA key that will return an AbstractMatrix
     */
    public CDMAKey generateKeyDimensionValue(URI uri, String[] path, String name) {
        // complete the path with dimension name
        String[] fullPath = Arrays.copyOf(path, path.length + 1);
        fullPath[fullPath.length - 1] = name;

        CDMAKey key = new CDMAKey(uri, CommandData.DimensionValue, fullPath);
        key.registerProperty(DIMENSION_NAME, name);
        key.registerProperty(NAV_MODE, this.mode);
        key.registerProperty(NAV_VIEW, this.view);
        key.setRank(1);
        if (pluginID != null) {
            key.setPlugin(pluginID);
        }
        return key;
    }

    /**
     * Create a key to list all item names on the targeted group
     * 
     * @param uri of the CDMA dataset to be opened
     * @param path of the CDMA group to be opened
     * 
     * @return CDMA key that will return a list of string
     */
    public CDMAKey generateKeyItemList(URI uri, String... path) {
        CDMAKey key = new CDMAKey(uri, CommandContainer.ItemList, path);
        key.registerProperty(NAV_MODE, this.mode);
        key.registerProperty(NAV_VIEW, this.view);
        key.setRank(1);
        if (pluginID != null) {
            key.setPlugin(pluginID);
        }
        return key;
    }

    /**
     * Create a key to list all group names on the targeted group
     * 
     * @param uri of the CDMA dataset to be opened
     * @param path of the CDMA group to be opened
     * 
     * @return CDMA key that will return a list of string
     */
    public CDMAKey generateKeySourceTitle(URI uri) {
        CDMAKey key = new CDMAKey(uri, CommandContainer.SourceName, new String[] {});
        key.registerProperty(NAV_MODE, this.mode);
        key.registerProperty(NAV_VIEW, this.view);
        key.setRank(0);
        if (pluginID != null) {
            key.setPlugin(pluginID);
        }
        return key;
    }

    /**
     * Create a key to set an attribute of the given name and value on a
     * targeted node.
     * 
     * @param uri of the CDMA dataset to be opened
     * @param path of the CDMA group to be opened
     * @param name of the attribute to set
     * @param value of the attribute to set
     * 
     * @return CDMA key that will return a list of string
     */
    public CDMAKey generateKeySetAttribute(URI uri, String[] path, String name, Object value) {
        CDMAKey key = new CDMAKey(uri, CommandContainer.AddAttribute, path);
        key.registerProperty(NAV_MODE, this.mode);
        key.registerProperty(NAV_VIEW, this.view);
        key.registerProperty(ATTRIBUTE_NAME, name);
        key.registerProperty(ATTRIBUTE_VALUE, value);
        if (pluginID != null) {
            key.setPlugin(pluginID);
        }
        return key;
    }

    // ---------------------------------------------------------
    // Data management methods
    // ---------------------------------------------------------
    /**
     * Create a key to get the value of a named attribute of the targeted node
     * 
     * @param uri of the CDMA dataset to be opened
     * @param path of the CDMA node to be opened
     * @param name of the attribute
     * 
     * @return CDMA key that will return an object value
     */
    public CDMAKey generateKeyAttributeValue(URI uri, String[] path, String name) {
        CDMAKey key = new CDMAKey(uri, CommandData.Attribute, path);
        key.registerProperty(ATTRIBUTE_NAME, name);
        key.registerProperty(NAV_MODE, this.mode);
        key.registerProperty(NAV_VIEW, this.view);
        if (pluginID != null) {
            key.setPlugin(pluginID);
        }
        return key;
    }

    /**
     * Create a key to get the matrix value of a targeted item
     * 
     * @param uri of the CDMA dataset to be opened
     * @param path of the CDMA item to be opened
     * 
     * @return CDMA key that will return an AbstractMatrix
     */
    public CDMAKey generateKeyArrayValue(URI uri, String... path) {
        CDMAKey key = new CDMAKey(uri, CommandData.Value, path);
        key.registerProperty(NAV_MODE, this.mode);
        key.registerProperty(NAV_VIEW, this.view);
        if (pluginID != null) {
            key.setPlugin(pluginID);
        }
        return key;
    }

    /**
     * Create a key to get the rank of a targeted matrix
     * 
     * @param uri of the CDMA dataset to be opened
     * @param path of the CDMA item to be opened
     * 
     * @return CDMA key that will return an integer value
     */
    public CDMAKey generateKeyArrayRank(URI uri, String... path) {
        CDMAKey key = new CDMAKey(uri, CommandData.Rank, path);
        key.registerProperty(NAV_MODE, this.mode);
        key.registerProperty(NAV_VIEW, this.view);
        key.setRank(0);
        if (pluginID != null) {
            key.setPlugin(pluginID);
        }
        return key;
    }

    /**
     * Create a key to get the shape of a targeted matrix
     * 
     * @param uri of the CDMA dataset to be opened
     * @param path of the CDMA item to be opened
     * 
     * @return CDMA key that will return an array of int
     */
    public CDMAKey generateKeyArrayShape(URI uri, String... path) {
        CDMAKey key = new CDMAKey(uri, CommandData.Shape, path);
        key.registerProperty(NAV_MODE, this.mode);
        key.registerProperty(NAV_VIEW, this.view);
        key.setRank(1);
        if (pluginID != null) {
            key.setPlugin(pluginID);
        }
        return key;
    }

    /**
     * Create a key to get a slice of a targeted matrix (as an AbstractMatrix)
     * 
     * @param uri of the CDMA dataset to be opened
     * @param path of the CDMA item to be opened
     * @param dimension on which to slice the matrix
     * @param position on which index of dimensions should the slice be done
     * 
     * @return CDMA key that will return an AbstractMatrix
     */
    public CDMAKey generateKeyArraySlice(URI uri, String[] path, int dimension, int position) {
        CDMAKey key = new CDMAKey(uri, CommandData.Slice, path);
        key.registerProperty(INT_DIM, dimension);
        key.registerProperty(INT_POS, position);
        key.registerProperty(NAV_MODE, this.mode);
        key.registerProperty(NAV_VIEW, this.view);
        if (pluginID != null) {
            key.setPlugin(pluginID);
        }
        return key;
    }

    /**
     * Create a key to get a region of a targeted matrix (as an AbstractMatrix)
     * 
     * @param uri of the CDMA dataset to be opened
     * @param path of the CDMA item to be opened
     * 
     * @return CDMA key that will return an AbstractMatrix
     */
    public CDMAKey generateKeyArrayRegion(URI uri, String[] path, int[] shape, int[] origin) {
        CDMAKey key = new CDMAKey(uri, CommandData.Region, path);
        key.registerProperty(ARRAY_ORIGIN, origin);
        key.registerProperty(ARRAY_SHAPE, shape);
        key.registerProperty(NAV_MODE, this.mode);
        key.registerProperty(NAV_VIEW, this.view);
        if (pluginID != null) {
            key.setPlugin(pluginID);
        }
        return key;
    }

    // ---------------------------------------------------------
    // URI management methods
    // ---------------------------------------------------------
    /**
     * Create a key to list available URIs that are under the given one.
     * The corresponding data source will return an AbstractMatrix<LabelledURI>
     * 
     * @param uri URI to scan
     * @param SortMode sort mode to apply (date or name)
     * @return CDMAKey that aims to list the sub URI
     */
    public CDMAKey generateKeyListValidURI(URI uri) {
        return generateKeyListValidURI(uri, SortMode.DATE);
    }

    /**
     * Create a key to list available URIs that are under the given one.
     * The corresponding data source will return an AbstractMatrix<LabelledURI>
     * 
     * @param uri URI to scan
     * @return CDMAKey that aims to list the sub URI
     */
    public CDMAKey generateKeyListValidURI(URI uri, SortMode sortingMode) {
        CDMAKey key = new CDMAKey(uri, CommandURI.GetValidURI);
        key.registerProperty(NAV_MODE, this.mode);
        key.registerProperty(NAV_VIEW, this.view);
        key.registerProperty(SORT_MODE, sortingMode);
        key.setRank(1);
        if (pluginID != null) {
            key.setPlugin(pluginID);
        }
        return key;
    }

    /**
     * Create a key to have the parent URI of the given one.
     * The corresponding data source will return a LabelledURI
     * 
     * @param uri URI to scan
     * @return CDMAKey that aims to get the parent URI
     */
    public CDMAKey generateKeyGetParentURI(URI uri) {
        CDMAKey key = new CDMAKey(uri, CommandURI.GetParentURI);
        key.registerProperty(NAV_MODE, this.mode);
        key.registerProperty(NAV_VIEW, this.view);
        key.setRank(0);
        if (pluginID != null) {
            key.setPlugin(pluginID);
        }
        return key;
    }

    /**
     * Create a key to tell if the given URI is explorable (i.e contains sub URI).
     * The corresponding data source will return a boolean
     * 
     * @param uri URI to scan
     * @return CDMAKey
     */
    public CDMAKey generateKeyIsBrowsableURI(URI uri) {
        CDMAKey key = new CDMAKey(uri, CommandURI.IsBrowsable);
        key.registerProperty(NAV_MODE, this.mode);
        key.registerProperty(NAV_VIEW, this.view);
        key.setRank(0);
        if (pluginID != null) {
            key.setPlugin(pluginID);
        }
        return key;
    }

    /**
     * Create a key to tell if the given URI is readable by at least a CDMA plug-in.
     * The corresponding data source will return a boolean
     * 
     * @param uri URI to scan
     * @return CDMAKey
     */
    public CDMAKey generateKeyIsReadableURI(URI uri) {
        CDMAKey key = new CDMAKey(uri, CommandURI.IsReadable);
        key.registerProperty(NAV_MODE, this.mode);
        key.registerProperty(NAV_VIEW, this.view);
        key.setRank(0);
        if (pluginID != null) {
            key.setPlugin(pluginID);
        }
        return key;
    }

    /**
     * Create a key to tell if the given URI is seen as an experiment by at least a CDMA plug-in.
     * The corresponding data source will return a boolean
     * 
     * @param uri URI to scan
     * @return CDMAKey
     */
    public CDMAKey generateKeyIsExperimentURI(URI uri) {
        CDMAKey key = new CDMAKey(uri, CommandURI.IsExperiment);
        key.registerProperty(NAV_MODE, this.mode);
        key.registerProperty(NAV_VIEW, this.view);
        key.setRank(0);
        if (pluginID != null) {
            key.setPlugin(pluginID);
        }
        return key;
    }

    /**
     * Add a filter to the key that will be interpreted by the CDMA, only when the browsing mode is
     * set to BrowsingMode.LOGICAL .
     * 
     * @param key to which filter will be added
     * @param filter to be applied
     */
    public void addFilterToKey(CDMAKey key, IFilter filter) {
        // Update the key
        if (filter != null) {
            List<IFilter> filters = key.getFilters();
            filters.add(filter);
        }
    }

    /**
     * Definitely close the underlying dataset (until the next access is requested)
     * 
     * @param key whom dataset must be closed
     */
    public static void unregisterDataset(CDMAKey key) {
        SourceFactory.unregisterDataset(key);
    }

    // ---------------------------------------------------------
    // Enum
    // ---------------------------------------------------------
    public enum BrowsingMode {
        PHYSICAL("Physical"), LOGICAL("Logical");

        private String mName;

        private BrowsingMode(String type) {
            mName = type;
        }

        public String getName() {
            return mName;
        }

        @Override
        public String toString() {
            return this.name();
        }
    }

    // ---------------------------------------------------------
    // Enum
    // ---------------------------------------------------------
    public enum SortMode {
        NAME("Name"), DATE("Date");

        private String mName;

        private SortMode(String type) {
            mName = type;
        }

        public String getName() {
            return mName;
        }

        @Override
        public String toString() {
            return this.name();
        }
    }
}
