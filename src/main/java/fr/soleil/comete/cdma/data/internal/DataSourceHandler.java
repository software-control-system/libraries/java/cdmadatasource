/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.cdma.data.internal;

import java.net.URI;
import java.util.Collection;

import org.cdma.Factory;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IDatasource;

public class DataSourceHandler {
    private long mLastModDate;
    private final IContainer mContainer;
    private final URI mURI;

    public DataSourceHandler(IContainer node) {
        mLastModDate = node == null ? 0 : node.getLastModificationDate();
        mContainer = node;
        mURI = null;
    }

    public DataSourceHandler(URI uri) {
        mLastModDate = 0;
        mContainer = null;
        mURI = uri;

        long last;
        Collection<IDatasource> list = Factory.getDatasources();
        for (IDatasource source : list) {
            last = source.getLastModificationDate(uri);
            if (last > mLastModDate) {
                mLastModDate = last;
            }
        }
    }

    public boolean isUptodate() {
        long last = 0;
        boolean result = true;

        if (mContainer != null) {
            last = mContainer.getLastModificationDate();
        } else if (mURI != null) {
            Collection<IDatasource> list = Factory.getDatasources();
            long tmp = 0;
            for (IDatasource source : list) {
                tmp = source.getLastModificationDate(mURI);
                if (tmp > last) {
                    last = tmp;
                }
            }
        }

        if (last > mLastModDate) {
            result = false;
        }
        return result;
    }

    public long getLastModificationDate() {
        isUptodate();
        return mLastModDate;
    }

    public IContainer getNode() {
        return mContainer;
    }

    public URI getURI() {
        return mURI;
    }
}
