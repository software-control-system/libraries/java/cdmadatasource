/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.cdma.data.adapter;

import javax.activation.UnsupportedDataTypeException;

import org.cdma.utilities.LabelledURI;

import fr.soleil.comete.cdma.data.factories.URISourceFactory.LabelledURIMatrix;
import fr.soleil.data.adapter.AbstractAdapter;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.exception.DataAdaptationException;

public class LabelledURIObjectAdapter extends AbstractAdapter<AbstractMatrix<LabelledURI>, Object[]> {

    public LabelledURIObjectAdapter(Class<?> firstType, Class<?> secondType) {
        super(firstType, secondType);
    }

    @Override
    public Object[] adapt(AbstractMatrix<LabelledURI> data) throws DataAdaptationException {
        Object[] result = null;
        if (data != null) {
            result = data.getValue();
        }
        return result;
    }

    @Override
    public AbstractMatrix<LabelledURI> revertAdapt(Object[] data) throws DataAdaptationException {
        LabelledURIMatrix result = null;
        if (data != null && data instanceof LabelledURI[][]) {
            result = new LabelledURIMatrix();
            try {
                result.setValue(data);
            } catch (UnsupportedDataTypeException e) {
                throw new DataAdaptationException(e);
            }
        }
        return result;
    }

}
