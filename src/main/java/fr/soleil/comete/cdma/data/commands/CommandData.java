/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.cdma.data.commands;

/**
 * Commands to work on data
 * 
 * @author rodriguez
 */
public enum CommandData implements Command {
    DimensionValue("DimensionValue"), Value("Value"), Attribute("Attribute"), Slice("Slice"), Region("Region"), Shape(
            "Shape"), Rank("Rank");

    private String mName;

    private CommandData(String type) {
        mName = type;
    }

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public CommandType getType() {
        return CommandType.CommandData;
    }

    @Override
    public boolean isSettable() {
        return false;
    }

    @Override
    public String toString() {
        return this.name();
    }
}
