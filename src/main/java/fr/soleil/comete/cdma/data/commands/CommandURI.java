/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.cdma.data.commands;

/**
 * Commands to identify given URI source
 * 
 * @author rodriguez
 */
public enum CommandURI implements Command {
    IsBrowsable("IsBrowsable", false), IsReadable("IsReadable", false), IsExperiment("IsExperiment", false),
    GetParentURI("GetParentURI", true), GetValidURI("GetValidURI", true),
    ListAvailablePlugins("ListAvailablePlugins", true);

    private final String name;
    private final boolean settable;

    private CommandURI(String type, boolean settable) {
        this.name = type;
        this.settable = settable;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public CommandType getType() {
        return CommandType.CommandURI;
    }

    @Override
    public boolean isSettable() {
        return settable;
    }

    @Override
    public String toString() {
        return getName();
    }
}
