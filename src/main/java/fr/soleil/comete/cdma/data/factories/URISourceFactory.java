/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.cdma.data.factories;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.activation.UnsupportedDataTypeException;

import org.cdma.interfaces.IDatasource;
import org.cdma.utilities.LabelledURI;
import org.cdma.utilities.comparator.LabelledURIDateComparator;
import org.cdma.utilities.comparator.LabelledURINameComparator;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.cdma.data.commands.CommandURI;
import fr.soleil.comete.cdma.data.internal.DataSourceHandler;
import fr.soleil.comete.cdma.data.keys.CDMAKey;
import fr.soleil.comete.cdma.data.keys.KeyProperties;
import fr.soleil.comete.cdma.data.service.CDMAKeyFactory.SortMode;
import fr.soleil.comete.cdma.data.source.CDMADataSource;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.container.matrix.StringMatrix;

public class URISourceFactory implements KeyProperties {
    public static class LabelledURIMatrix extends AbstractMatrix<LabelledURI> {

        public LabelledURIMatrix() {
            super(LabelledURI.class);
        }

        @Override
        protected Class<LabelledURI> generateDefaultDataType() {
            return LabelledURI.class;
        }

    };

    public static CDMADataSource<?> createDataSource(CDMAKey key) {
        CDMADataSource<?> result = null;
        CommandURI command = (CommandURI) key.getCommand();

        try {
            Class<?> type = getDataType(command);
            DataSourceHandler observer = SourceFactory.getObserver(key, true);
            result = SourceFactory.createSource(key, type, observer);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    // ---------------------------------------------------------
    // protected methods
    // ---------------------------------------------------------
    /**
     * Return the result of the command specified in the key executed by the
     * given IContainer
     * 
     * @param node
     *            that should execute the command
     * @param k
     *            CDMAKey with data source and path informations
     * @return the IContainer to be processed
     * @throws Exception
     *             if any problem
     */
    protected static Object processCommand(URI uri, CDMAKey key) {
        Object result = null;
        Collection<IDatasource> sources = SourceFactory.getSources(key);
        if (key.getCommand() instanceof CommandURI) {
            CommandURI command = (CommandURI) key.getCommand();

            switch (command) {
                case IsBrowsable: {
                    result = false;
                    for (IDatasource source : sources) {
                        if (source.isBrowsable(uri)) {
                            result = true;
                            break;
                        }
                    }
                    break;
                }
                case IsExperiment: {
                    result = false;
                    for (IDatasource source : sources) {
                        if (source.isExperiment(uri)) {
                            result = true;
                            break;
                        }
                    }
                    break;
                }
                case IsReadable: {
                    result = false;
                    for (IDatasource source : sources) {
                        if (source.isReadable(uri)) {
                            result = true;
                            break;
                        }
                    }
                    break;
                }
                case GetValidURI: {
                    // Use LinkedHashMap to keep potential data sorting
                    Map<String, LabelledURI> allUris = new LinkedHashMap<String, LabelledURI>();
                    for (IDatasource source : sources) {
                        LabelledURI labelledURI;
                        List<URI> uris = source.getValidURI(uri);
                        for (URI subURI : uris) {
                            if (!allUris.containsKey(subURI.toString())) {
                                labelledURI = new LabelledURI(subURI, source);
                                allUris.put(subURI.toString(), labelledURI);
                            }
                        }
                    }

                    Comparator<LabelledURI> comparator = new LabelledURIDateComparator();
                    Object sortMode = key.getPropertyValue(SORT_MODE);
                    if (SortMode.NAME.equals(sortMode)) {
                        comparator = new LabelledURINameComparator();
                    }

                    Set<LabelledURI> uris = new TreeSet<LabelledURI>(comparator);
                    for (LabelledURI labelledURI : allUris.values()) {
                        uris.add(labelledURI);
                    }

                    result = uris.toArray(new LabelledURI[uris.size()]);
                    allUris.clear();
                    uris.clear();
                    break;
                }
                case GetParentURI: {
                    result = null;

                    IDatasource datasource = null;
                    for (IDatasource source : sources) {
                        if (source.isProducer(uri)) {
                            datasource = source;
                            break;
                        } else if (source.isBrowsable(uri)) {
                            datasource = source;
                        }
                    }

                    if (datasource != null) {
                        URI newURI = datasource.getParentURI(uri);
                        result = new LabelledURI(newURI, datasource);
                    }
                    break;
                }
                case ListAvailablePlugins: {
                    List<String> plugins = new ArrayList<String>();
                    for (IDatasource source : sources) {
                        plugins.add(source.getFactoryName());
                    }
                    result = plugins.toArray(new String[sources.size()]);
                    break;
                }

                default:
                    LoggerFactory.getLogger(URISourceFactory.class.getName())
                            .info("Unknown command to process: " + command.getName());
                    break;
            }
        }
        return result;
    }

    /**
     * Convert the given data into a compatible data structure expected by the
     * command
     * 
     * @param data
     * @param command
     * @return
     */
    protected static Object convertData(Object data, CommandURI command) {
        Object result = null;
        if (data != null) {
            switch (command) {
                case GetValidURI:
                    LabelledURIMatrix labelledURIMatrix = new LabelledURIMatrix();
                    try {
                        labelledURIMatrix.setFlatValue(data, 1, ((LabelledURI[]) data).length);
                    } catch (UnsupportedDataTypeException e) {
                    }
                    result = labelledURIMatrix;
                    break;
                case ListAvailablePlugins:
                    StringMatrix stringMatrix = new StringMatrix();
                    stringMatrix.setFlatValue((String[]) data, 1, ((String[]) data).length);
                    result = stringMatrix;
                    break;
                case GetParentURI:
                case IsBrowsable:
                case IsExperiment:
                case IsReadable:
                default:
                    result = data;
                    break;
            }
        }
        return result;
    }

    // ---------------------------------------------------------
    // private methods
    // ---------------------------------------------------------
    /**
     * Return the expected data type for the given command
     * 
     * @param command
     * @note it aims to make possible the use of generics
     */
    private static Class<?> getDataType(CommandURI command) {
        Class<?> clazz;
        switch (command) {
            case GetValidURI:
                clazz = LabelledURI[].class;
                break;
            case GetParentURI:
                clazz = LabelledURI.class;
                break;
            case IsBrowsable:
            case IsExperiment:
            case IsReadable:
                clazz = Boolean.class;
                break;
            case ListAvailablePlugins:
                clazz = String[].class;
                break;
            default:
                clazz = Object.class;
                break;
        }
        return clazz;
    }
}
