/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.cdma.data.adapter;

import javax.activation.UnsupportedDataTypeException;

import org.cdma.utilities.LabelledURI;

import fr.soleil.comete.cdma.data.factories.URISourceFactory.LabelledURIMatrix;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.data.adapter.AbstractAdapter;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.exception.DataAdaptationException;

public class LabelledURIMatrixTreeNodeAdapter extends AbstractAdapter<AbstractMatrix<LabelledURI>, ITreeNode> {

    private final LabelledURIArrayTreeNodeAdapter adapter;

    public LabelledURIMatrixTreeNodeAdapter(Class<?> firstType, Class<?> secondType) {
        super(firstType, secondType);
        adapter = new LabelledURIArrayTreeNodeAdapter(LabelledURI[].class, ITreeNode.class);
    }

    @Override
    public ITreeNode adapt(AbstractMatrix<LabelledURI> data) throws DataAdaptationException {
        ITreeNode result = null;
        if (data != null) {
            LabelledURI[] arrayUri = (LabelledURI[]) data.getFlatValue();
            result = adapter.adapt(arrayUri);
        }
        return result;
    }

    @Override
    public AbstractMatrix<LabelledURI> revertAdapt(ITreeNode data) throws DataAdaptationException {
        LabelledURIMatrix result = null;
        if (data != null) {
            LabelledURI[] array = adapter.revertAdapt(data);
            if (array != null) {
                result = new LabelledURIMatrix();
                try {
                    result.setFlatValue(array, 1, array.length);
                } catch (UnsupportedDataTypeException e) {
                    throw new DataAdaptationException(e);
                }
            }
        }
        return result;
    }

}
