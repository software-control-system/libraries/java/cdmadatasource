/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.cdma.data.source;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.activation.UnsupportedDataTypeException;

import org.cdma.exception.CDMAException;
import org.cdma.interfaces.IArray;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IDataItem;
import org.cdma.utilities.LabelledURI;
import org.cdma.utils.ArrayTools;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.cdma.data.commands.CommandData;
import fr.soleil.comete.cdma.data.factories.SourceFactory;
import fr.soleil.comete.cdma.data.internal.CDMAExceptionHandler;
import fr.soleil.comete.cdma.data.internal.DataSourceHandler;
import fr.soleil.comete.cdma.data.keys.CDMAKey;
import fr.soleil.comete.cdma.data.service.CDMAKeyFactory;
import fr.soleil.comete.cdma.data.service.CDMAKeyFactory.BrowsingMode;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.container.matrix.StringMatrix;
import fr.soleil.data.exception.InvalidSourceDataException;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.plugin.AbstractPlugin;
import fr.soleil.data.plugin.FormatPlugin;
import fr.soleil.data.plugin.UnitPlugin;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.source.BufferedDataSource;
import fr.soleil.data.source.IPluggableDataSource;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * Default data source for CDMA
 * 
 * @param <T> The type of data manipulated by this source
 */
public class CDMADataSource<T> extends BufferedDataSource<T> implements AutoCloseable, IPluggableDataSource {

    private static final String NULL = String.valueOf((Object) null);
    private static final String FAILED_TO_EXTRACT_SHAPE_FROM_KEY = "Failed to extract shape from key: ";
    private static final String FAILED_TO_READ_DATA_FROM_KEY = "Failed to read  data from key: ";
    private static final String SPACE = " ";
    private static final String OUT_OF_MEMORY_ERROR_FOR_DIMENSION = " Out Of Memory Error for dimension ";
    private static final String FORMAT = "format";
    private static final String UNITS = "units";

    private DataSourceHandler observer;
    private final GenericDescriptor descriptor;
    private final CDMAKey key;
    private volatile boolean initialized;
    private int[] shape;
    private WeakReference<T> weakData;
    protected final List<AbstractPlugin<?>> plugins;
    protected volatile boolean pluginsInitialized;

    public CDMADataSource(CDMAKey key, GenericDescriptor descriptor, DataSourceHandler observer) {
        super(key, false, true);
        this.key = (CDMAKey) key.clone();
        this.observer = observer;
        this.descriptor = descriptor;
        initialized = false;
        weakData = null;
        plugins = new ArrayList<>();
        pluginsInitialized = false;
    }

    @Override
    public T getData() throws Exception {
        CDMAExceptionHandler.resetError();
        T result = ObjectUtils.recoverObject(weakData);
        if ((!initialized) || (result == null)) {
            result = readData();
            weakData = new WeakReference<>(result);
        }
        CDMAException lastError = CDMAExceptionHandler.getLastError();
        if (lastError != null) {
            CDMAExceptionHandler.resetError();
            throw lastError;
        }
        return result;
    }

    @Override
    protected void updateData(boolean ignoreDuplication) throws Exception {
        boolean changed;
        if ((!initialized) || (weakData == null) || ((observer != null) && (!observer.isUptodate()))) {
            weakData = new WeakReference<>(readData());
            changed = true;
        } else {
            changed = false;
        }
        initWrittenData(writtenData);
        if (ignoreDuplication || changed) {
            notifyMediators();
        }
    }

    @Override
    public GenericDescriptor getDataType() {
        return descriptor;
    }

    @Override
    public boolean isSettable() {
        return key.isSettable();
    }

    @SuppressWarnings("unchecked")
    @Override
    protected T readData() throws Exception {
        T value = ObjectUtils.recoverObject(weakData);
        if (initialized) {
            initialized = (value != null); // force data reading if reference was cleaned
        }
        CDMAExceptionHandler.resetError();
        try {
            if (!observer.isUptodate()) {
                SourceFactory.unregisterDataset(key);
            }
            if ((!observer.isUptodate()) || (!initialized)) {
                initialized = true;
                observer = SourceFactory.getObserver(key);
                int[] shape = this.shape;
                Object rawData = SourceFactory.processCommand(key, observer);
                // Add unsigned information see JIRA EXPDATA-274
                IContainer node = observer.getNode();
                boolean unsigned;
                if (node instanceof IDataItem) {
                    IDataItem item = (IDataItem) node;
                    unsigned = item.isUnsigned();
                    if (shape == null) {
                        shape = item.getShape();
                    }
                } else {
                    unsigned = false;
                }
                value = (T) SourceFactory.convertData(rawData, key, unsigned);
                if (shape == null) {
                    if (rawData instanceof IArray) {
                        shape = ((IArray) rawData).getShape();
                    } else {
                        shape = ArrayTools.detectShape(rawData);
                    }
                    if ((shape != null) && (shape.length == 1) && (shape[0] == 1)) {
                        shape = ArrayUtils.EMPTY_SHAPE;
                    }
                }
                this.shape = shape;
            }
        } catch (Exception e) {
            String infoKey = key == null ? NULL : key.getInformationKey();
            String errorMessage = FAILED_TO_READ_DATA_FROM_KEY + infoKey + SPACE + e.getMessage();
            CDMAExceptionHandler.handleCDMAException(key.getSource(), e);
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(errorMessage, e);
            throw e;
        } catch (OutOfMemoryError mem) {
            String shapeValue = shape == null ? NULL : Arrays.toString(shape);
            String infoKey = key == null ? NULL : key.getInformationKey();
            String errorMessage = FAILED_TO_READ_DATA_FROM_KEY + infoKey + OUT_OF_MEMORY_ERROR_FOR_DIMENSION
                    + shapeValue;
            CDMAExceptionHandler.handleCDMAException(key.getSource(), mem);
            LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(errorMessage, mem);
            throw mem;
        }
        CDMAException lastError = CDMAExceptionHandler.getLastError();
        if (lastError != null) {
            CDMAExceptionHandler.resetError();
            throw lastError;
        }
        return value;
    }

    @Override
    protected void writeData(T data) throws InvalidSourceDataException {
        CDMAKey key = this.key;
        if (data != null) {
            if (key.isSettable()) {
                switch (key.getCommand().getType()) {
                    case CommandURI:
                        if ((data instanceof AbstractMatrix<?>)
                                && ((AbstractMatrix<?>) data).getType().equals(LabelledURI.class)) {
                            LabelledURI[] uris = (LabelledURI[]) ((AbstractMatrix<?>) data).getFlatValue();
                            if ((uris != null) && (uris.length > 0)) {
                                LabelledURI uri = uris[0];
                                if (uri != null) {
                                    key.setDataSource(uri.getURI());
                                    // Set as not initialized to force next update
                                    initialized = false;
                                }
                            }
                        }
                        break;
                    case CommandContainer:
                        if (data instanceof StringMatrix) {
                            String name = ((StringMatrix) data).getFlatValue()[0];
                            String[] path = new String[key.getPath().length + 1];
                            int i = 0;
                            for (String node : key.getPath()) {
                                if (!node.isEmpty()) {
                                    path[i] = node;
                                    i++;
                                }
                            }
                            path[path.length - 1] = name;
                            key.setPath(path);
                            // Set as not initialized to force next update
                            initialized = false;
                        }
                        break;
                    case CommandData:
                        break;
                    default:
                        break;
                }
            }
        } else {
            // Reset the super key with mKey values
            CDMAKey origin = (CDMAKey) getOriginDescriptor();
            key.setCommand(origin.getCommand());
            key.setDataSource(origin.getSource());
            key.setPath(origin.getPath());
            key.setKeyFilter(origin.getFilters());
            // Set as not initialized to force next update
            initialized = false;
        }
    }

    protected int[] recoverShape() throws Exception {
        int[] shape = this.shape;
        if ((shape == null) || (!observer.isUptodate())) {
            CDMAExceptionHandler.resetError();
            try {
                if (!observer.isUptodate()) {
                    SourceFactory.unregisterDataset(key);
                    observer = SourceFactory.getObserver(key);
                    initialized = false; // observer was not up to date: should force reading again
                    IContainer node = observer.getNode();
                    if (node instanceof IDataItem) {
                        shape = ((IDataItem) node).getShape();
                    }
                }
            } catch (Exception e) {
                String infoKey = key == null ? NULL : key.getInformationKey();
                String errorMessage = FAILED_TO_EXTRACT_SHAPE_FROM_KEY + infoKey + SPACE + e.getMessage();
                CDMAExceptionHandler.handleCDMAException(key.getSource(), e);
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(errorMessage, e);
                throw e;
            } catch (OutOfMemoryError mem) {
                String shapeValue = shape == null ? NULL : Arrays.toString(shape);
                String infoKey = key == null ? NULL : key.getInformationKey();
                String errorMessage = FAILED_TO_EXTRACT_SHAPE_FROM_KEY + infoKey + OUT_OF_MEMORY_ERROR_FOR_DIMENSION
                        + shapeValue;
                CDMAExceptionHandler.handleCDMAException(key.getSource(), mem);
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(errorMessage, mem);
                throw mem;
            }
            CDMAException lastError = CDMAExceptionHandler.getLastError();
            if (lastError != null) {
                CDMAExceptionHandler.resetError();
                throw lastError;
            }
        }
        return shape;
    }

    public int[] getShape() {
        if ((shape == null) || (!observer.isUptodate())) {
            try {
                // recover shape without reading data if possible
                shape = recoverShape();
                if (shape == null) {
                    readData();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return shape;
    }

    public int getRank() {
        int[] shape = getShape();
        int rank;
        if (shape == null) {
            rank = -1;
        } else if ((shape.length == 1) && (shape[0] == 1)) {
            rank = 0;
        } else {
            rank = shape.length;
        }
        return rank;
    }

    @Override
    public void close() {
        SourceFactory.closeDataset(key);
        clearData(ObjectUtils.recoverObject(weakData));
        weakData = null;
        clearData(data);
        data = null;
        clearData(writtenData);
        writtenData = null;
    }

    protected void clearData(Object data) {
        if (data instanceof Collection<?>) {
            ((Collection<?>) data).clear();
        } else if (data instanceof Map<?, ?>) {
            ((Map<?, ?>) data).clear();
        } else if (data instanceof AbstractMatrix<?>) {
            try {
                ((AbstractMatrix<?>) data).setFlatValue(null, 0, 0);
            } catch (UnsupportedDataTypeException e) {
                // Ignore: won't happen and not that important
            }
        }
    }

    @Override
    public List<AbstractPlugin<?>> getListPlugin() {
        List<AbstractPlugin<?>> result = new ArrayList<>();
        synchronized (plugins) {
            if (!pluginsInitialized) {
                initPlugins();
            }
            result.addAll(plugins);
        }
        return result;
    }

    private void initPlugins() {
        try {
            IContainer node = observer.getNode();
            if ((node instanceof IDataItem) && CommandData.Value.equals(key.getCommand())) {
                // CTRLRFC-184: apply format and unit
                BrowsingMode mode = key.getMode();
                CDMAKeyFactory factory = new CDMAKeyFactory(key.getView());
                factory.enableMode(mode);
                CDMAKey formatKey = factory.generateKeyAttributeValue(observer.getURI(), key.getPath(), FORMAT);
                formatKey.setPlugin(key.getPlugin());
                formatKey.setDataSource(key.getSource());
                plugins.add(new FormatPlugin(formatKey));
                CDMAKey unitKey = factory.generateKeyAttributeValue(observer.getURI(), key.getPath(), UNITS);
                unitKey.setPlugin(key.getPlugin());
                unitKey.setDataSource(key.getSource());
                plugins.add(new UnitPlugin(unitKey));
            }
        } catch (Exception e) {
            // nothing to do
        } finally {
            pluginsInitialized = true;
        }

    }
}