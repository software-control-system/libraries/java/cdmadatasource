/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.cdma.data.commands;

/**
 * Commands to work on containers
 * 
 * @author rodriguez
 */
public enum CommandContainer implements Command {
    DimensionList("DimensionList"), GroupList("GroupList"), ItemList("ItemList"), AttributeList(
            "AttributeList"), AddAttribute("AddAttribute"), SourceName("SourceName");

    private String mName;

    private CommandContainer(String type) {
        mName = type;
    }

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public CommandType getType() {
        return CommandType.CommandContainer;
    }

    @Override
    public boolean isSettable() {
//        boolean result = false;
//
//        switch (this) {
//            case DimensionList:
//            case AttributeList:
//            case GroupList:
//            case ItemList:
//            case SourceName:
//            default:
//                result = true;
//                break;
//        }
//        return result;
        return true;
    }

    @Override
    public String toString() {
        return this.name();
    }
}
