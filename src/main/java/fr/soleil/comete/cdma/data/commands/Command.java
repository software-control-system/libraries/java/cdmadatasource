/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.cdma.data.commands;

public interface Command {
    public String getName();

    public CommandType getType();

    public boolean isSettable();

    @Override
    public String toString();

    // Type of available commands
    public enum CommandType {
        CommandURI("CommandURI"), CommandContainer("CommandContainer"), CommandData("CommandData");

        private final String mName;

        private CommandType(String type) {
            mName = type;
        }

        public String getName() {
            return mName;
        }
    }
}
