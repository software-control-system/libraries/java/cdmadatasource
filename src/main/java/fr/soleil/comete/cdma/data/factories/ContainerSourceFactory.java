/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.cdma.data.factories;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.cdma.Factory;
import org.cdma.IFactory;
import org.cdma.dictionary.LogicalGroup;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IDimension;
import org.cdma.interfaces.IGroup;
import org.cdma.utils.Utilities.ModelType;

import fr.soleil.comete.cdma.data.commands.CommandContainer;
import fr.soleil.comete.cdma.data.internal.DataSourceHandler;
import fr.soleil.comete.cdma.data.keys.CDMAKey;
import fr.soleil.comete.cdma.data.keys.KeyProperties;
import fr.soleil.comete.cdma.data.source.CDMADataSource;
import fr.soleil.data.container.matrix.StringMatrix;

public class ContainerSourceFactory implements KeyProperties {

    public static CDMADataSource<?> createDataSource(CDMAKey key) {
        CDMADataSource<?> result = null;
        CommandContainer command = (CommandContainer) key.getCommand();

        Class<?> type = getDataType(command);

        try {
            DataSourceHandler observer = SourceFactory.getObserver(key, true);

            result = SourceFactory.createSource(key, type, observer);
        } catch (Exception e) {
            Factory.getLogger().error("Unable to create data source!", e);
        }

        return result;
    }

    /**
     * Convert the given data into a compatible data structure expected by the
     * command
     * 
     * @param data
     * @param command
     * @return
     */
    protected static Object convertData(Object data, CommandContainer command) {
        Object result = null;
        if (data != null) {
            switch (command) {
                case DimensionList:
                case GroupList:
                case ItemList:
                case AttributeList:
                    String[] array = (String[]) data;
                    StringMatrix matrix = new StringMatrix();
                    matrix.setFlatValue(array, 1, array.length);
                    result = matrix;
                    break;
                case SourceName:
//                    result = new String((String) data);
                    result = data;
                    break;
                default:
                    result = null;
                    break;
            }
        }
        return result;
    }

    /**
     * Return the result of the command specified in the key executed by the
     * given IContainer
     * 
     * @param node
     *            that should execute the command
     * @param k
     *            CDMAKey with data source and path informations
     * @return the IContainer to be processed
     * @throws Exception
     *             if any problem
     */
    protected static Object processCommand(IContainer node, CDMAKey key) {
        Object result = null;

        if (key.getCommand() instanceof CommandContainer) {
            CommandContainer command = (CommandContainer) key.getCommand();
            if (node != null) {
                switch (command) {
                    case GroupList: {
                        List<String> names = new ArrayList<String>();
                        if (node instanceof IGroup) {
                            Collection<IGroup> nodes = ((IGroup) node).getGroupList();
                            for (IGroup group : nodes) {
                                names.add(group.getShortName());
                            }
                        } else if (node instanceof LogicalGroup) {
                            LogicalGroup group = ((LogicalGroup) node);
                            List<String> keys = group.getKeyNames(ModelType.LogicalGroup);
                            names.addAll(keys);
                        }
                        result = names.toArray(new String[] {});
                        break;
                    }
                    case ItemList: {
                        List<String> names = new ArrayList<String>();
                        if (node instanceof IGroup) {
                            Collection<IDataItem> nodes = ((IGroup) node).getDataItemList();
                            for (IDataItem item : nodes) {
                                names.add(item.getShortName());
                            }
                        } else if (node instanceof LogicalGroup) {
                            LogicalGroup group = ((LogicalGroup) node);

                            List<String> keys = group.getKeyNames(ModelType.DataItem);
                            for (String keyName : keys) {
                                IDataItem item = group.getDataItem(keyName);
                                if (item != null) {
                                    names.add(keyName);
                                }
                            }

                            /*
                             * List<String> keys =
                             * group.getKeyNames(ModelType.DataItem);
                             * names.addAll(keys);
                             */
                        }
                        result = names.toArray(new String[] {});
                        break;
                    }
                    case DimensionList: {
                        List<String> names = new ArrayList<String>();
                        if (node instanceof IGroup) {
                            List<IDimension> nodes = ((IGroup) node).getDimensionList();
                            for (IDimension item : nodes) {
                                names.add(item.getName());
                            }
                        } else if (node instanceof LogicalGroup) {
                            LogicalGroup group = ((LogicalGroup) node);
                            List<String> keys = group.getKeyNames(ModelType.Dimension);
                            names.addAll(keys);
                        }
                        result = names.toArray(new String[] {});
                        break;
                    }
                    case AttributeList: {
                        Collection<IAttribute> attributes = node.getAttributeList();
                        List<String> names = new ArrayList<String>();
                        for (IAttribute attr : attributes) {
                            names.add(attr.getName());
                        }
                        result = names.toArray(new String[] {});
                        break;
                    }
                    case AddAttribute: {
                        String attrName = key.getPropertyValue(ATTRIBUTE_NAME).toString();
                        Object attrValue = key.getPropertyValue(ATTRIBUTE_VALUE);
                        IFactory factory = Factory.getFactory(node.getFactoryName());
                        IAttribute attr = factory.createAttribute(attrName, attrValue);
                        node.addOneAttribute(attr);
                        result = attr;
                        break;
                    }
                    case SourceName: {
                        result = node.getDataset().getTitle();
                        break;
                    }
                }
            }
        }
        return result;
    }

    // ---------------------------------------------------------
    // private methods
    // ---------------------------------------------------------
    /**
     * Return the expected data type for the given command
     * 
     * @param command
     * @note it aims to make possible the use of generics
     */
    private static Class<?> getDataType(CommandContainer command) {
        Class<?> clazz;
        switch (command) {
            case AttributeList:
            case GroupList:
            case ItemList:
                clazz = String[].class;
                break;
            case SourceName:
                clazz = String.class;
                break;
            default:
                clazz = Object.class;
                break;
        }
        return clazz;
    }
}
