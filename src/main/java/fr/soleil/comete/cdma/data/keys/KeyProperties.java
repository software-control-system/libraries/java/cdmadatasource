/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.cdma.data.keys;

public interface KeyProperties {
    // Browsing mechanism (physical or logical)
    public static final String NAV_MODE = "BrowsingMode";
    public static final String NAV_VIEW = "BrowsingView";

    // Common part of each CDMAKey
    public static final String KEY_SOURCE = "KeySource";
    public static final String KEY_PATH = "KeyPath";
    public static final String KEY_COMMAND = "KeyCommand";
    public static final String KEY_EXPECTED_RANK = "KeyExpectedRank";
    public static final String KEY_PLUGIN = "KeyPlugin";

    // Properties for data sorting
    public static final String SORT_MODE = "SortMode";

    // Properties for attribute
    public static final String DIMENSION_NAME = "DimensionName";
    public static final String ATTRIBUTE_NAME = "AttributeName";
    public static final String ATTRIBUTE_VALUE = "AttributeValue";

    // Properties for array operations
    public static final String ARRAY_ORIGIN = "ArrayOrigin";
    public static final String ARRAY_SHAPE = "ArrayShape";
    public static final String INT_DIM = "ParamDim";
    public static final String INT_POS = "ParamPos";

    // Array of all properties for a CDMA key
    public static final String[] PROPERTIES = new String[] { NAV_MODE, NAV_VIEW, KEY_SOURCE, KEY_PATH, KEY_COMMAND,
            KEY_EXPECTED_RANK, KEY_PLUGIN, DIMENSION_NAME, ATTRIBUTE_NAME, ATTRIBUTE_VALUE, ARRAY_ORIGIN, ARRAY_SHAPE,
            INT_DIM, INT_POS, SORT_MODE };

    // Parsing/Generating CDMAKey into XML
    public static final String CELLS_SEPARATOR = "-,-";
    public static final String XML_TAG_NAME = "property";
    public static final String XML_ATT_NAME = "name";
    public static final String XML_ATT_VALUE = "value";
}
