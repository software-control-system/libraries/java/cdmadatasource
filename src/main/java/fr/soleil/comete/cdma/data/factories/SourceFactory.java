/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.cdma.data.factories;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cdma.Factory;
import org.cdma.IFactory;
import org.cdma.dictionary.Key;
import org.cdma.dictionary.LogicalGroup;
import org.cdma.dictionary.filter.IFilter;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IDataset;
import org.cdma.interfaces.IDatasource;
import org.cdma.interfaces.IGroup;

import fr.soleil.comete.cdma.data.adapter.ExternalAdapters;
import fr.soleil.comete.cdma.data.commands.Command;
import fr.soleil.comete.cdma.data.commands.CommandContainer;
import fr.soleil.comete.cdma.data.commands.CommandData;
import fr.soleil.comete.cdma.data.commands.CommandURI;
import fr.soleil.comete.cdma.data.internal.DataSourceHandler;
import fr.soleil.comete.cdma.data.keys.CDMAKey;
import fr.soleil.comete.cdma.data.keys.KeyProperties;
import fr.soleil.comete.cdma.data.service.CDMAKeyFactory.BrowsingMode;
import fr.soleil.comete.cdma.data.source.CDMADataSource;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.service.GenericDescriptor;

public final class SourceFactory implements KeyProperties {
    // Register various adapter
    static {
        ExternalAdapters.register();
    }

    // Map: location => [plugin => dataset]
    private static final Map<String, Map<String, IDataset>> DATASETS = new HashMap<>();
    // Map: location => [plugin => dataset counter]
    private static final Map<String, Map<String, Integer>> COUNTERS = new HashMap<>();

    public static CDMADataSource<?> createDataSource(CDMAKey key) {
        CDMADataSource<?> source = null;
        switch (key.getCommand().getType()) {
            case CommandContainer:
                source = ContainerSourceFactory.createDataSource(key);
                break;
            case CommandData:
                source = ArraySourceFactory.createDataSource(key);
                break;
            case CommandURI:
                source = URISourceFactory.createDataSource(key);
                break;
            default:
                break;
        }
        return source;
    }

    protected static <T> CDMADataSource<T> createSource(CDMAKey key, Class<T> type, DataSourceHandler observer) {
        CDMADataSource<T> result = null;
        if (type != null) {
            result = new CDMADataSource<T>(key, getDataType(type), observer);
        }
        return result;
    }

    /**
     * According the given key and an observer will trigger the execution of the process
     * corresponding to the given key.
     * 
     * @param key containing informations of the datasource
     * @param observer containing actor to process command on
     * @return the result of the command
     */
    public static Object processCommand(CDMAKey key, DataSourceHandler observer) {
        Object data = null;
        Command command = key.getCommand();
        switch (command.getType()) {
            case CommandContainer:
                data = ContainerSourceFactory.processCommand(observer.getNode(), key);
                break;
            case CommandData:
                data = ArraySourceFactory.processCommand(observer.getNode(), key);
                break;
            case CommandURI:
                data = URISourceFactory.processCommand(observer.getURI(), key);
                break;
            default:
                break;
        }
        return data;
    }

    public static Object convertData(Object inData, CDMAKey key, boolean unsigned) {
        Object outData = null;
        Command command = key.getCommand();
        switch (command.getType()) {
            case CommandContainer:
                outData = ContainerSourceFactory.convertData(inData, (CommandContainer) key.getCommand());
                break;
            case CommandData:
                // Add unsigned information see JIRA EXPDATA-274
                outData = ArraySourceFactory.convertData(inData, (CommandData) key.getCommand(), unsigned);
                break;
            case CommandURI:
                outData = URISourceFactory.convertData(inData, (CommandURI) key.getCommand());
                break;
            default:
                break;
        }
        return outData;
    }

    /**
     * Return the datasource's handler corresponding to the given key
     * 
     * @param key of the datasource
     * @return DataSourceHandler
     * @throws Exception
     */
    public static DataSourceHandler getObserver(CDMAKey key) throws Exception {
        return getObserver(key, false);
    }

    protected static DataSourceHandler getObserver(CDMAKey key, boolean updateCounter) throws Exception {
        DataSourceHandler result = null;
        switch (key.getCommand().getType()) {
            case CommandContainer:
            case CommandData:
                result = new DataSourceHandler(getContainer(key, updateCounter));
                break;
            case CommandURI:
                result = new DataSourceHandler(key.getSource());
                break;
        }

        return result;
    }

    public static void unregisterDataset(CDMAKey key) {
        if (key != null) {
            URI uri = key.getSource();
            String plugin = key.getPlugin();
            unregisterDataset(uri.toString(), plugin);
        }
    }

    public static void unregisterDataset(String location, String plugin) {
        if ((location != null) && (plugin != null)) {
            synchronized (DATASETS) {
                Map<String, IDataset> datasets = DATASETS.get(location);
                Map<String, Integer> counters = COUNTERS.get(location);
                if (datasets != null) {
                    datasets.remove(plugin);
                    counters.remove(plugin);
                    if (datasets.isEmpty()) {
                        DATASETS.remove(location);
                        COUNTERS.remove(location);
                    }
                }
            }
        }
    }

    public static Collection<IDatasource> getSources(CDMAKey key) {
        Collection<IDatasource> sources;
        String plugin = key.getPlugin();

        if ((plugin != null) && !plugin.isEmpty()) {
            sources = new ArrayList<IDatasource>();
            IFactory factory = Factory.getFactory(plugin);
            if (factory != null) {
                sources.add(factory.getPluginURIDetector());
            }
        } else {
            sources = Factory.getDatasources();
        }

        return sources;
    }

    static public boolean isOpenedDataset(URI uri) {
        boolean result = false;

        // Check if that URI as already been opened as it is
        if (uri != null) {
            IDataset dataset = getDataset(uri, null, false);
            result = (dataset != null);
        }
        return result;
    }

    public static void closeDataset(CDMAKey key) {
        if (key != null) {
            // Get the dataset
            IDataset dataset = getDataset(key, false);
            if (dataset != null) {
                Integer counter = 1;
                String location = key.getSource().toString();
                String plugin = dataset.getFactoryName();

                synchronized (DATASETS) {
                    // Get the corresponding ref counter
                    Map<String, Integer> counters = COUNTERS.get(location);
                    if (counters != null) {
                        if (counters.containsKey(plugin)) {
                            counter = counters.get(plugin) - 1;

                            // Update ref counters
                            if (counter > 0) {
                                counters.put(plugin, counter);
                            }
                        }
                    }
                }
                if (counter <= 0) {
                    unregisterDataset(location, plugin);
                }
            }
        }

    }

    // ------------------------------------------------------------------------
    // Protected methods
    // ------------------------------------------------------------------------
    /**
     * Return the right GenericDescriptor of the given object
     * 
     * @param <T>
     * 
     * @param data
     * @return
     */
    protected static GenericDescriptor getDataType(Class<?> clazz) {
        GenericDescriptor result = null;
        if (clazz != null) {
            if (clazz.isArray()) {
                result = new GenericDescriptor(AbstractMatrix.class, new GenericDescriptor(clazz.getComponentType()));
            } else {
                result = new GenericDescriptor(clazz);
            }
        }
        return result;
    }

    // ------------------------------------------------------------------------
    // Private methods
    // ------------------------------------------------------------------------
    /**
     * Return the IContainer on which a CDMA treatment is expected
     * 
     * @param key CDMAKey with data source and path informations
     * @param updateCounter will the opening of the dataset increment the reference counter
     * @return the IContainer to be processed
     * @throws Exception if any problem
     */
    private static IContainer getContainer(CDMAKey key, boolean updateCounter) throws Exception {
        String view = (String) key.getPropertyValue(NAV_VIEW);
        BrowsingMode mode = (BrowsingMode) key.getPropertyValue(NAV_MODE);
        IDataset dataset = null;
        IContainer result = null;

//        synchronized (SourceFactory.class) {
        // Store the previously set view
//            String oldView = Factory.getActiveView();

        // Get the operating CDMA IContainer
//            if (!view.isEmpty()) {
//                Factory.setActiveView(view);
//            }
        dataset = openDataset(key, updateCounter);
        if (dataset != null) {
            if (mode == BrowsingMode.LOGICAL) {
                result = dataset.getLogicalRoot(view);
            } else {
                result = dataset.getRootGroup();
            }
        }

        // Open all node until we reach the last one in path
        if (result != null) {
            // Logical parsing
            String[] path = key.getPath();

            if (mode == BrowsingMode.LOGICAL) {
                // Add the filters to the last key
                List<IFilter> filters = key.getFilters();
                IFactory factory = Factory.getFactory(result.getFactoryName());
                Key dictionaryKey;
                IContainer container;

                // Seek the last not empty node
                int i = 0;
                int last = 0;
                for (String node : path) {
                    if (!node.isEmpty()) {
                        last++;
                    }
                }

                for (String name : path) {
                    if (!name.isEmpty()) {
                        dictionaryKey = new Key(factory, name);

                        // Last key to get item
                        if (++i == last) {
                            // Add all filters to the key
                            for (IFilter filter : filters) {
                                dictionaryKey.pushFilter(filter);
                            }
                        }

                        // if result is a LogicalGroup get its child
                        if (result instanceof LogicalGroup) {
                            container = ((LogicalGroup) result).getGroup(dictionaryKey);

                            if (container == null) {
                                container = ((LogicalGroup) result).getDataItem(dictionaryKey);
                            }
                            result = container;
                        } else {
                            result = null;
                            break;
                        }
                    }
                }
            } else {
                String dimName = null;
                if (CommandData.DimensionValue.equals(key.getCommand())) {
                    dimName = (String) key.getPropertyValue(KeyProperties.DIMENSION_NAME);
                }

                // Physical parsing
                for (String name : path) {
                    if (!name.isEmpty()) {
                        if (result instanceof IGroup) {
                            // Avoid the IDimension when path contains a dimension name
                            if ((dimName == null) || !dimName.equals(name)) {
                                result = ((IGroup) result).getContainer(name);
                            }
                        } else {
                            result = null;
                            break;
                        }
                    }
                }
            }
        }

//            // Restore the previously set view
//            Factory.setActiveView(oldView);
//
//        }

        return result;
    }

    private static IDataset openDataset(CDMAKey key, boolean updateCounter) {

        IDataset result = SourceFactory.getDataset(key, updateCounter);

        if (result == null) {
            result = SourceFactory.instantiateDataset(key);
        }

        return result;
    }

    private static IDataset instantiateDataset(CDMAKey key) {
        // Check if a plugin is specified
        String plugin = key.getPlugin();
        IDataset result = null;

        // Get the requested URI
        URI uri = key.getSource();
        if (uri != null) {
            String location = uri.toString();
            synchronized (DATASETS) {
                Map<String, IDataset> datasets = DATASETS.get(location);
                Map<String, Integer> counters = new HashMap<>();
                // Check the soft ref is still alive
                if (datasets == null) {
                    datasets = new HashMap<>();
                    DATASETS.put(location, datasets);
                    COUNTERS.put(location, counters);
                }

                try {
                    // Check if a specific plug-in is required
                    if ((plugin == null) || plugin.isEmpty()) {
                        result = Factory.openDataset(uri);
                    } else {
                        IFactory factory = Factory.getFactory(plugin);
                        if (factory != null) {
                            result = factory.openDataset(uri);
                        }
                    }
                    // Update the map of opened dataset and counters
                    if (result != null) {
                        plugin = result.getFactoryName();
                        datasets.put(plugin, result);
                        counters.put(plugin, 1);
                    }
                } catch (Exception e) {
                    Factory.getLogger().info("Cannot instantiate dataset", e);
                }
            }
        }
        return result;
    }

    /**
     * Return a dataset instance if it has already been created
     * 
     * @param key
     * @return dataset or null if not found
     */
    private static IDataset getDataset(CDMAKey key, boolean updateCounter) {
        IDataset result = null;

        // Get the requested URI
        URI uri = key.getSource();

        // Check if a plugin is specified
        String plugin = key.getPlugin();

        // Search the dataset
        result = getDataset(uri, plugin, updateCounter);

        return result;
    }

    /**
     * Return a dataset instance if it has already been created
     * 
     * @param uri location of the requested dataset
     * @param plugin name of the requested dataset
     * @return dataset or null if not found
     * @note if plugin is null then the first found dataset for that location will be returned
     */
    private static IDataset getDataset(URI uri, String plugin, boolean updateCounter) {
        IDataset result = null;
        if (uri != null) {
            String location = uri.toString();
            synchronized (DATASETS) {
                // Get all opened dataset for that location
                Map<String, IDataset> datasets = DATASETS.get(location);
                if (datasets != null) {
                    if ((plugin != null) && (!plugin.isEmpty())) {
                        // If a plug-in is specified check the right one is available
                        result = datasets.get(plugin);
                    } else {
                        // No plug-in specified: take the first available dataset
                        for (IDataset dataset : datasets.values()) {
                            if (dataset != null) {
                                result = dataset;
                                break;
                            }
                        }
                    }
                }
                if ((result != null) && updateCounter) {
                    Map<String, Integer> counters = COUNTERS.get(location);
                    if (counters == null) {
                        counters = new HashMap<>();
                        counters.put(result.getFactoryName(), 0);
                        COUNTERS.put(location, counters);
                    }
                    Integer counter = counters.get(result.getFactoryName());
                    counters.put(result.getFactoryName(), ++counter);
                }
            }
        }
        return result;
    }
}
