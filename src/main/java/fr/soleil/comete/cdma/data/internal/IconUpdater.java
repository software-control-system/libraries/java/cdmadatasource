/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.cdma.data.internal;

import java.lang.ref.WeakReference;
import java.net.URI;

import org.cdma.Factory;
import org.cdma.IFactory;
import org.cdma.interfaces.IDatasource;
import org.cdma.utilities.LabelledURI;
import org.cdma.utilities.performance.PostTreatment;

import fr.soleil.comete.cdma.data.adapter.LabelledURITreeNodeAdapter;
import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.lib.project.ObjectUtils;

public class IconUpdater implements PostTreatment {

    public static final CometeImage UNKNOWN_IMAGE = getImage("/com/famfamfam/silk/connect.png");
    public static final CometeImage EXPERIMENT_IMAGE = getImage(
            "/fr/soleil/comete/cdma/data/icons/lightExperiment.png");
    public static final CometeImage BROWSABLE_IMAGE = getImage("/com/famfamfam/silk/folder.png");
    public static final CometeImage WRITING_IMAGE = getImage("/com/famfamfam/silk/drive_edit.png");
    public static final CometeImage COMPUTING_IMAGE = getImage("/com/famfamfam/silk/cog.png");
    public static final CometeImage READABLE_IMAGE = getImage("/com/famfamfam/silk/package.png");

    public static final String CDMA_RELOAD_UNKNOWN_FILES = "CDMA_RELOAD_UNKNOWN_FILES";

    private LabelledURI data;
    private final WeakReference<ITreeNode> nodeRef;
    private final Object lock;

    // private final boolean enableTryAgain;

    // private static Map<URI, Long> lastModificationDates = new HashMap<URI, Long>();

    // private final long threadSleepTime = 1500;

    public IconUpdater(LabelledURI data, ITreeNode node) {
        this.data = data;
        this.nodeRef = node == null ? null : new WeakReference<ITreeNode>(node);
        this.lock = new Object();
        // this.enableTryAgain = Boolean.valueOf(System.getProperty(CDMA_RELOAD_UNKNOWN_FILES, "true"));
    }

    protected static final CometeImage getImage(String path) {
        CometeImage image;
        try {
            image = new CometeImage(LabelledURITreeNodeAdapter.class.getResource(path).toURI());
        } catch (Exception e) {
            Factory.getLogger().warn("Unable to load icon: " + path);
            image = null;
        }
        return image;
    }

    @Override
    public String getName() {
        return "Data source icon updater";
    }

    @Override
    public void process() {
        LabelledURI data = this.data;
        ITreeNode node = ObjectUtils.recoverObject(nodeRef);
        if ((data != null) && (node != null)) {
            // boolean retryAgain = true;
            URI uri = data.getURI();
            IDatasource source = data.getDatasource();

            synchronized (lock) {
                node.setImage(COMPUTING_IMAGE);
                if (!source.isProducer(uri)) {
                    IFactory factory = Factory.getFactory(uri);
                    if (factory != null) {
                        source = factory.getPluginURIDetector();
                    }
                }
                if (source != null) {

                    // System.out.println("Processing " + uri.toASCIIString() + " with " + source.getFactoryName());

                    if (source.isReadable(uri)) {
                        node.setImage(READABLE_IMAGE);
                        // retryAgain = false;
                        if (source.isExperiment(uri)) {
                            node.setImage(EXPERIMENT_IMAGE);
                            // retryAgain = false;
                        }
                    } else if (source.isBrowsable(uri)) {
                        node.setImage(BROWSABLE_IMAGE);
                        // retryAgain = false;
                    } else {
                        node.setImage(UNKNOWN_IMAGE);
                    }
                    // if (source != null) {
                    //
                    // long lastModificationDate = source.getLastModificationDate(uri);
                    // Long storedLastModificationDate = lastModificationDates.get(uri);
                    //
                    // if (storedLastModificationDate == null || (lastModificationDate > storedLastModificationDate)) {
                    // lastModificationDates.put(uri, lastModificationDate);
                    // System.out.println("Processing " + uri.toASCIIString() + " with " + source.getFactoryName());
                    // if (source.isExperiment(uri)) {
                    // node.setImage(EXPERIMENT_IMAGE);
                    // retryAgain = false;
                    // } else {
                    // if (source.isBrowsable(uri)) {
                    // if (source.isReadable(uri)) {
                    // node.setImage(READABLE_IMAGE);
                    // retryAgain = false;
                    // } else {
                    // node.setImage(BROWSABLE_IMAGE);
                    // retryAgain = false;
                    // }
                    // } else {
                    // node.setImage(UNKNOWN_IMAGE);
                    // }
                    // }
                    // } else {
                    // if (newSource) {
                    // node.setImage(UNKNOWN_IMAGE);
                    // retryAgain = false;
                    // }
                    // }
                    // }
                    // }
                    // if (enableTryAgain && retryAgain) {
                    // PostTreatmentManager.registerTreatment(this, 10000);
                    // }

                }
            }
        }
        this.data = null;
    }

    @Override
    public String toString() {
        ITreeNode node = ObjectUtils.recoverObject(nodeRef);
        return node == null ? null : node.getName();
    }

}
