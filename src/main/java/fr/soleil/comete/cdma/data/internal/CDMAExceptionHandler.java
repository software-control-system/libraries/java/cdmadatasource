package fr.soleil.comete.cdma.data.internal;

import org.cdma.exception.CDMAException;
import org.cdma.utilities.CDMAExceptionManager;
import org.cdma.utilities.ICDMAExceptionHandler;

public class CDMAExceptionHandler implements ICDMAExceptionHandler {

    private static final CDMAExceptionHandler HANDLER = new CDMAExceptionHandler();

    private static CDMAException lastError = null;
    private static Object lastSource = null;

    public static void registerToCDMAExceptionManager() {
        CDMAExceptionManager.addCDMAExceptionHandler(HANDLER);
    }

    public static void handleCDMAException(Object source, Throwable exception) {
        HANDLER.handleCDMAException(source, new CDMAException(exception));
    }

    public static void resetError() {
        lastError = null;
        lastSource = null;
    }

    @Override
    public void handleCDMAException(Object source, CDMAException cdmaException) {
        lastError = cdmaException;
        lastSource = source;
    }

    public static CDMAException getLastError() {
        return lastError;
    }

    public static Object getLastSource() {
        return lastSource;
    }
}
