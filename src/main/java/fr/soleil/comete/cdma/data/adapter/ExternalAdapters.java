/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.cdma.data.adapter;

import org.cdma.utilities.LabelledURI;

import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.data.adapter.AdapterUtils;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.service.GenericDescriptor;

public class ExternalAdapters {
    // Scalar descriptors
    private static final GenericDescriptor TREENODE = new GenericDescriptor(ITreeNode.class);
    private static final GenericDescriptor STRING = new GenericDescriptor(String.class);
    private static final GenericDescriptor LABELLED_URI = new GenericDescriptor(LabelledURI.class);
    // Arrays descriptors
    private static final GenericDescriptor LABELLED_URI_ARRAY = new GenericDescriptor(LabelledURI[].class);
    private static final GenericDescriptor OBJECT_ARRAY = new GenericDescriptor(Object[].class);
    // Matrices descriptors
    private static final GenericDescriptor LABELLED_URI_ABSTRACT_MATRIX = new GenericDescriptor(AbstractMatrix.class,
            new GenericDescriptor(LabelledURI.class));
    private static final GenericDescriptor STRING_ABSTRACT_MATRIX = new GenericDescriptor(AbstractMatrix.class,
            new GenericDescriptor(String.class));

    static public void register() {
        AdapterUtils.registerExternalAdapter(LABELLED_URI, STRING, LabelledURIAdapter.class);
        AdapterUtils.registerExternalAdapter(LABELLED_URI, TREENODE, LabelledURITreeNodeAdapter.class);
        AdapterUtils.registerExternalAdapter(LABELLED_URI_ARRAY, TREENODE, LabelledURIArrayTreeNodeAdapter.class);
        AdapterUtils.registerExternalAdapter(LABELLED_URI_ABSTRACT_MATRIX, STRING_ABSTRACT_MATRIX,
                LabelledURIMatrixAdapter.class);
        AdapterUtils
                .registerExternalAdapter(LABELLED_URI_ABSTRACT_MATRIX, OBJECT_ARRAY, LabelledURIObjectAdapter.class);
        AdapterUtils.registerExternalAdapter(LABELLED_URI_ABSTRACT_MATRIX, TREENODE,
                LabelledURIMatrixTreeNodeAdapter.class);
    }
}
