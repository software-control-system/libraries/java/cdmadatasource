/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.cdma.data.adapter;

import java.net.URI;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.cdma.interfaces.IDatasource;
import org.cdma.utilities.LabelledURI;

import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.data.adapter.AbstractAdapter;
import fr.soleil.data.exception.DataAdaptationException;

public class LabelledURIArrayTreeNodeAdapter extends AbstractAdapter<LabelledURI[], ITreeNode> {

    private final LabelledURITreeNodeAdapter adapter;

    public LabelledURIArrayTreeNodeAdapter(Class<?> firstType, Class<?> secondType) {
        super(firstType, secondType);
        adapter = new LabelledURITreeNodeAdapter(LabelledURI.class, ITreeNode.class);
    }

    @Override
    public ITreeNode adapt(LabelledURI[] data) throws DataAdaptationException {
        ITreeNode result = null;
        if (data != null) {
            ITreeNode[] children = new ITreeNode[data.length];
            // Use sorted URIs to build parent node
            Map<URI, IDatasource> uriMap = new TreeMap<URI, IDatasource>();
            if (data.length > 0) {
                URI uri = data[0].getDatasource().getParentURI(data[0].getURI());
                if (uri != null) {
                    // Instantiate ITreeNodes
                    for (int i = 0; i < data.length; i++) {
                        if ((data[i] != null) && (data[i].getURI() != null) && (data[i].getDatasource() != null)) {
                            children[i] = adapter.adapt(data[i]);
                            uriMap.put(uri, data[i].getDatasource());
                        }
                    }
                }
            }

            if (!uriMap.isEmpty()) {
                URI parentUri = null;
                IDatasource parentDatasource = null;
                String[] parentParts = null;
                // Retrieve parent URI and IDatasource of found children
                for (Entry<URI, IDatasource> entry : uriMap.entrySet()) {
                    parentUri = entry.getKey();
                    parentDatasource = entry.getValue();
                    if (parentDatasource == null) {
                        parentUri = null;
                    } else {
                        parentParts = parentDatasource.getURIParts(parentUri);
                        if (parentParts == null) {
                            parentUri = null;
                            parentDatasource = null;
                        } else {
                            break;
                        }
                    }
                }

                uriMap.clear();
                // create parent node and add children nodes
                if ((parentUri != null) && (parentDatasource != null)) {
                    result = adapter.adapt(new LabelledURI(parentUri, parentDatasource));
                    result.addNodes(children);
                    if (result != null) {
                        for (ITreeNode child : children) {
                            child.setParent(result);
                        }
                    }
                }
            }
        }
        return result;
    }

    @Override
    public LabelledURI[] revertAdapt(ITreeNode data) throws DataAdaptationException {
        LabelledURI[] result = null;
        if (data != null) {
            LabelledURI uri = adapter.revertAdapt(data);
            if (uri != null) {
                result = new LabelledURI[] { uri };
            }
        }
        return result;
    }

}
