/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.cdma.data.keys;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.cdma.Factory;
import org.cdma.IFactory;
import org.cdma.dictionary.filter.IFilter;
import org.cdma.interfaces.IContainer;
import org.cdma.utilities.conversion.ArrayConverters;
import org.cdma.utilities.conversion.ArrayConverters.StringArrayConverter;
import org.cdma.utils.Utilities.ModelType;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import fr.soleil.comete.cdma.data.commands.Command;
import fr.soleil.comete.cdma.data.commands.Command.CommandType;
import fr.soleil.comete.cdma.data.commands.CommandContainer;
import fr.soleil.comete.cdma.data.commands.CommandData;
import fr.soleil.comete.cdma.data.commands.CommandURI;
import fr.soleil.comete.cdma.data.factories.SourceFactory;
import fr.soleil.comete.cdma.data.internal.DataSourceHandler;
import fr.soleil.comete.cdma.data.service.CDMADataSourceFactory;
import fr.soleil.comete.cdma.data.service.CDMAKeyFactory.BrowsingMode;
import fr.soleil.data.exception.KeyCompatibilityException;
import fr.soleil.data.service.AbstractKey;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.xmlhelpers.XMLLine;

/**
 * 
 * @author rodriguez
 */
public class CDMAKey extends AbstractKey implements KeyProperties {

    private static final String NULL = "null";
    private static final String SPACE = " ";
    private static final String KEY_SEPARATOR = ":";
    private static final String PATH_SEPARATOR = "/";
    private static final String PROPERTY_START = "[";
    private static final String PROPERTY_END = "]";
    private static final String PROPERTY_END_EQUAL = "]=";
    private static final String NEW_LINE = "\n";
    private static final List<String> KEY_PROPERTIES = Arrays.asList(PROPERTIES);

    static {
        DataSourceProducerProvider.pushNewProducer(CDMADataSourceFactory.class);
    }

    private List<IFilter> filters;

    public CDMAKey() {
        this(null, null);
    }

    public CDMAKey(URI source, Command command, String... path) {
        super(CDMADataSourceFactory.SOURCE_PRODUCER_ID);
        setDataSource(source);
        setCommand(command);
        setPath(path);
        setRank(-1);
        setPlugin(ObjectUtils.EMPTY_STRING);
        filters = new ArrayList<>();
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public String getInformationKey() {
        StringBuilder buf = new StringBuilder();

        buf.append(KEY_SOURCE);
        buf.append(String.valueOf(getSource()));

        buf.append(SPACE);
        buf.append(KEY_COMMAND);
        buf.append(KEY_SEPARATOR);
        Command cmd = getCommand();
        buf.append(cmd == null ? null : cmd.getName());

        buf.append(SPACE);
        buf.append(KEY_PATH);
        buf.append(KEY_SEPARATOR);
        for (String node : getPath()) {
            buf.append(PATH_SEPARATOR);
            buf.append(node);
        }

        return buf.toString();
    }

    public void setDataSource(URI uri) {
        registerProperty(KEY_SOURCE, uri);
    }

    public void setPath(String... path) {
        Path p = new Path(path);
        registerProperty(KEY_PATH, p);
    }

    public void setCommand(Command command) {
        registerProperty(KEY_COMMAND, command);
    }

    public URI getSource() {
        return (URI) getPropertyValue(KEY_SOURCE);
    }

    public String[] getPath() {
        return ((Path) getPropertyValue(KEY_PATH)).getValue();
    }

    public Command getCommand() {
        return (Command) getPropertyValue(KEY_COMMAND);
    }

    public void setKeyFilter(List<IFilter> filters) {
        this.filters = (filters == null ? new ArrayList<>() : filters);
    }

    public List<IFilter> getFilters() {
        return filters;
    }

    public void setRank(int rank) {
        registerProperty(KEY_EXPECTED_RANK, rank);
    }

    public void setPlugin(String pluginID) {
        registerProperty(KEY_PLUGIN, pluginID);
    }

    public String getPlugin() {
        String plugin = (String) getPropertyValue(KEY_PLUGIN);
        if ((plugin == null || plugin.isEmpty())) {
            URI uri = getSource();
            if (uri != null) {
                IFactory factory = Factory.getFactory(uri);
                plugin = factory == null ? ObjectUtils.EMPTY_STRING : factory.getName();
            }
        }
        return plugin;
    }

    public boolean isSettable() {
        return getCommand().isSettable();
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (String prop : this.getExistingProperties()) {
            result.append(PROPERTY_START).append(prop).append(PROPERTY_END_EQUAL).append(getPropertyValue(prop))
                    .append(NEW_LINE);
        }
        return result.toString();
    }

    @Override
    public void parseXML(Node node) throws KeyCompatibilityException {
        if (node != null) {
            NamedNodeMap attributes = node.getAttributes();
            if (attributes != null) {
                Node attribute;
                for (int index = 0; index < attributes.getLength(); index++) {
                    attribute = attributes.item(index);
                    registerProp(attribute.getNodeName(), attribute.getNodeValue());
                }
            }
        }
    }

    @Override
    public XMLLine[] toXMLLines(String tag) {
        XMLLine line = createOpeningOrEmptyLine(tag, true);

        Object valueObject;
        String value;

        for (String property : getExistingProperties()) {
            if (KEY_PROPERTIES.contains(property)) {
                valueObject = getPropertyValue(property);

                if (property.equals(ARRAY_ORIGIN) || property.equals(ARRAY_SHAPE)) {
                    value = convertArrayToString(valueObject);
                } else if (property.equals(KEY_PATH)) {
                    String[] s = ((Path) valueObject).getValue();
                    value = convertArrayToString(s);
                } else if (property.equals(KEY_COMMAND)) {
                    value = ((Command) valueObject).getType().toString() + CELLS_SEPARATOR
                            + ((Command) valueObject).toString();
                } else {
                    value = valueObject.toString();
                }
                line.setAttribute(property, value);
            }
        }
        return new XMLLine[] { line };
    }

    public boolean isGroup() {
        boolean result = false;

        try {
            DataSourceHandler handler = SourceFactory.getObserver(this);
            IContainer container = handler.getNode();
            if (container != null) {
                result = container.getModelType() == ModelType.Group;
            }
        } catch (Exception e) {
        }

        return result;
    }

    /**
     * Return the active mode of navigation for the corresponding source: Physical or Logical
     */
    public BrowsingMode getMode() {
        BrowsingMode result = (BrowsingMode) getPropertyValue(NAV_MODE);
        return result;
    }

    /**
     * Return the active view when the Logical is enabled
     */
    public String getView() {
        String result = (String) getPropertyValue(NAV_VIEW);
        return result;
    }

    // ------------------------------------------------------------------------
    // Private methods
    // ------------------------------------------------------------------------
    private String convertArrayToString(Object array) {
        Class<?> clazz = array.getClass();
        StringBuilder buffer = new StringBuilder();
        String[] values = new String[] {};
        buffer.append(PROPERTY_START);
        if (clazz.isArray()) {
            if (clazz.getComponentType().equals(String.class)) {
                values = (String[]) array;
            } else {
                while (clazz != null && clazz.isArray()) {
                    clazz = clazz.getComponentType();
                }
                if (clazz != null) {
                    StringArrayConverter converter = ArrayConverters.detectConverter(clazz);
                    values = converter.convert(array);
                }
            }

            int i = 0;
            for (String cell : values) {
                if (i != 0) {
                    buffer.append(CELLS_SEPARATOR);
                }
                i++;
                buffer.append(cell);
            }
        }
        buffer.append(PROPERTY_END);
        return buffer.toString();
    }

    @SuppressWarnings("unchecked")
    private <T> T[] convertStringToArray(final String sequence, Class<T> clazz) {
        Object result = null;
        if (sequence != null) {
            String buffer = sequence.substring(sequence.indexOf(PROPERTY_START) + 1,
                    sequence.lastIndexOf(PROPERTY_END));
            String[] array = buffer.split(CELLS_SEPARATOR);
            if ((array.length == 1) && array[0].isEmpty()) {
                array = new String[0];
            }
            if (clazz.equals(String.class)) {
                result = array;
            } else {
                result = ArrayUtils.newInstance(clazz, array.length);
                if (array.length > 0) {
                    StringArrayConverter converter = ArrayConverters.detectConverter(clazz);
                    converter.convert(array, result);
                }
            }
        }

        return (T[]) result;
    }

    private void registerProp(String name, String value) {
        if (KEY_PROPERTIES.contains(name)) {
            Object property;
            if ((value == null) || NULL.equals(value)) {
                property = null;
            } else {
                switch (name) {
                    case ARRAY_ORIGIN:
                        property = convertStringToArray(value, Integer.TYPE);
                        break;
                    case KEY_PATH:
                        property = new Path(convertStringToArray(value, String.class));
                        break;
                    case KEY_COMMAND:
                        String[] command = value.split(CELLS_SEPARATOR);
                        CommandType type = CommandType.valueOf(command[0]);
                        switch (type) {
                            case CommandContainer:
                                property = CommandContainer.valueOf(command[1]);
                                break;
                            case CommandData:
                                property = CommandData.valueOf(command[1]);
                                break;
                            case CommandURI:
                                property = CommandURI.valueOf(command[1]);
                                break;
                            default:
                                property = null;
                                break;
                        } // end switch (type)
                        break;
                    case INT_DIM:
                    case INT_POS:
                    case KEY_EXPECTED_RANK:
                        property = Integer.valueOf(value.trim());
                        break;
                    case KEY_SOURCE:
                        try {
                            property = new URI(value);
                        } catch (URISyntaxException e) {
                            property = null;
                        }
                        break;
                    case NAV_MODE:
                        property = BrowsingMode.valueOf(value);
                        break;
                    default:
                        property = value;
                        break;
                } // end switch (name)
            } // end if ((value == null) || NULL.equals(value)) ... else
            registerProperty(name, property);
        } // end if (KEY_PROPERTIES.contains(name))
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    private class Path {
        String[] mPath;

        public Path(String[] path) {
            mPath = path;
        }

        public String[] getValue() {
            return mPath;
        }

        @Override
        public boolean equals(Object obj) {
            boolean equals = true;
            if (obj instanceof Path) {
                if (obj != this) {
                    Path toCompare = (Path) obj;
                    equals = ArrayUtils.equals(mPath, toCompare.mPath);
                }
            } else {
                equals = false;
            }

            return equals;
        }

        @Override
        public int hashCode() {
            int code = 0;
            if (mPath != null) {
                for (String node : mPath) {
                    code += ObjectUtils.getHashCode(node);
                }
            }
            return code;
        }

        @Override
        public String toString() {
            StringBuilder result = new StringBuilder();
            if (mPath != null) {
                for (String node : mPath) {
                    result.append(PATH_SEPARATOR).append(node);
                }
            }
            return result.toString();
        }
    }

}
