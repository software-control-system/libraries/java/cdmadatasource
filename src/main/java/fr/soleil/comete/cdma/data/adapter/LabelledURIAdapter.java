/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.cdma.data.adapter;

import java.util.Collection;
import java.util.Collections;
import java.util.WeakHashMap;

import org.cdma.utilities.LabelledURI;

import fr.soleil.data.adapter.AbstractAdapter;
import fr.soleil.data.exception.DataAdaptationException;

public class LabelledURIAdapter extends AbstractAdapter<LabelledURI, String> {
    private static final Collection<LabelledURI> mConverted = Collections
            .newSetFromMap(new WeakHashMap<LabelledURI, Boolean>());

    public LabelledURIAdapter(Class<?> firstType, Class<?> secondType) {
        super(firstType, secondType);
    }

    @Override
    public String adapt(LabelledURI data) throws DataAdaptationException {
        String result = null;
        if (data != null) {
            synchronized (mConverted) {
                mConverted.add(data);
            }
            result = data.getLabel();
        }
        return result;
    }

    @Override
    public LabelledURI revertAdapt(String data) throws DataAdaptationException {
        LabelledURI result = null;
        if (data != null) {
            synchronized (mConverted) {
                for (LabelledURI uri : mConverted) {
                    if (uri.getLabel() == data) {
                        result = uri;
                        break;
                    } else if ((result == null) && data.equals(uri.getLabel())) {
                        result = uri;
                    }
                }
            }
        }
        return result;
    }

}
