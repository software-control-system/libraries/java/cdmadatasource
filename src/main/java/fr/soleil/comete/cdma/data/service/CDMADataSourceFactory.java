/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.cdma.data.service;

import fr.soleil.comete.cdma.data.factories.SourceFactory;
import fr.soleil.comete.cdma.data.internal.CDMAExceptionHandler;
import fr.soleil.comete.cdma.data.keys.CDMAKey;
import fr.soleil.comete.cdma.data.source.CDMADataSource;
import fr.soleil.data.service.AbstractRefreshingManager;
import fr.soleil.data.service.IKey;
import fr.soleil.data.source.AbstractDataSource;

public class CDMADataSourceFactory extends AbstractRefreshingManager<CDMAKey> {

    public static final String SOURCE_PRODUCER_ID = "fr.soleil.comete.cdma.data.service";
    public static final String LABEL = "CDMA";

    public CDMADataSourceFactory() {
        super();
        CDMAExceptionHandler.registerToCDMAExceptionManager();
    }

    @Override
    public String getId() {
        return SOURCE_PRODUCER_ID;
    }

    @Override
    public boolean isSourceSettable(IKey key) {
        return false;
    }

    @Override
    public boolean isSourceCreatable(IKey key) {
        return (key instanceof CDMAKey);
    }

    @Override
    public String getName() {
        return LABEL;
    }

    @Override
    public CDMAKey parseKey(IKey key) {
        CDMAKey parsedKey;
        if (key instanceof CDMAKey) {
            parsedKey = (CDMAKey) key;
        } else {
            parsedKey = null;
        }
        return parsedKey;
    }

    @Override
    protected CDMAKey createIdentifier(IKey key) {
        return parseKey(key);
    }

    @Override
    protected AbstractDataSource<?> constructDataSource(IKey key) {
        return SourceFactory.createDataSource(parseKey(key));
    }

    /**
     * Useless with CDMADataSource: no calculation available until the source is created
     */
    @Override
    public int[] getShape(IKey key) {
        int[] result = null;

        CDMAKey k = parseKey(key);
        if (k != null) {
            try (CDMADataSource<?> source = (CDMADataSource<?>) constructDataSource(k);) {
                if (source != null) {
                    result = source.getShape();
                }
            }

        }
        return result;
    }

    @Override
    public int getRank(IKey key) {
        int result = -1;

        CDMAKey k = parseKey(key);
        if (k != null) {
            try (CDMADataSource<?> source = (CDMADataSource<?>) constructDataSource(k);) {
                if (source != null) {
                    result = source.getRank();
                }
            }

        }
        return result;
    }

    /**
     * Clean the Data source: ask the factory to close source and unregister source
     * if it is not used anymore.
     */
    @Override
    protected void cleanDataSourceAfterRemoving(AbstractDataSource<?> source) {
        if (source instanceof CDMADataSource<?>) {
            CDMADataSource<?> src = (CDMADataSource<?>) source;
            src.close();
        }
    }

}
