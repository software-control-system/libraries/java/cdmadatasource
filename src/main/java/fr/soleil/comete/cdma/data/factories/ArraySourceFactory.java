/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.cdma.data.factories;

import javax.activation.UnsupportedDataTypeException;

import org.cdma.Factory;
import org.cdma.exception.DataAccessException;
import org.cdma.exception.InvalidRangeException;
import org.cdma.interfaces.IArray;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IContainer;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IDimension;
import org.cdma.interfaces.IGroup;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.cdma.data.commands.CommandData;
import fr.soleil.comete.cdma.data.internal.DataSourceHandler;
import fr.soleil.comete.cdma.data.keys.CDMAKey;
import fr.soleil.comete.cdma.data.keys.KeyProperties;
import fr.soleil.comete.cdma.data.service.CDMADataSourceFactory;
import fr.soleil.comete.cdma.data.source.CDMADataSource;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.container.matrix.BasicObjectMatrix;
import fr.soleil.data.container.matrix.BooleanMatrix;
import fr.soleil.data.container.matrix.ByteMatrix;
import fr.soleil.data.container.matrix.DoubleMatrix;
import fr.soleil.data.container.matrix.FloatMatrix;
import fr.soleil.data.container.matrix.IntMatrix;
import fr.soleil.data.container.matrix.LongMatrix;
import fr.soleil.data.container.matrix.ShortMatrix;
import fr.soleil.data.container.matrix.StringMatrix;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.math.UnsignedConverter;

/**
 * Manages all CDMA keys of type CommandData.
 * 
 * @author rodriguez
 */
public class ArraySourceFactory implements KeyProperties {
    public static CDMADataSource<?> createDataSource(final CDMAKey key) {
        CDMADataSource<?> result = null;

        try {
            DataSourceHandler observer = SourceFactory.getObserver(key, true);
            IContainer node = observer.getNode();
            if (node != null) {
                result = SourceFactory.createSource(key, getDataType(key, node), observer);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Return the result of the command specified in the key executed by the given IContainer
     * 
     * @param node that should execute the command
     * @param k CDMAKey with data source and path informations
     * @return the IContainer to be processed
     * @throws Exception if any problem
     */
    protected static Object processCommand(final IContainer node, final CDMAKey key) {
        Object result = null;
        if (node != null) {
            CommandData command = (CommandData) key.getCommand();
            switch (command) {
                case DimensionValue: {
                    if (node instanceof IGroup) {
                        String name = (String) key.getPropertyValue(DIMENSION_NAME);
                        IDimension dimension = ((IGroup) node).getDimension(name);
                        if (dimension != null) {
                            result = dimension.getCoordinateVariable();
                        }
                    }
                    break;
                }
                case Value: {
                    if (node instanceof IDataItem) {
                        try {
                            IDataItem item = ((IDataItem) node);
                            if (item.isScalar()
                                    && (item.getType().equals(Character.TYPE) || item.getType().equals(String.class))) {
                                result = item.readScalarString();
                            } else {
                                result = item.getData();
                            }
                        } catch (DataAccessException e) {
                            LoggerFactory.getLogger(CDMADataSourceFactory.class.getName())
                                    .info("Cannot read data" + e.getMessage());
                        }
                    }
                    break;
                }
                case Attribute: {
                    IAttribute attr = node.getAttribute((String) key.getPropertyValue(ATTRIBUTE_NAME));
                    if (attr != null) {
                        if (attr.isString()) {
                            result = attr.getStringValue();
                        } else if (!attr.isArray()) {
                            result = attr.getNumericValue();
                        } else {
                            result = attr.getValue();
                        }
                    }
                    break;
                }
                case Region: {
                    if (node instanceof IDataItem) {
                        IDataItem item = (IDataItem) node;
                        int[] origin = (int[]) key.getPropertyValue(ARRAY_ORIGIN);
                        int[] shape = (int[]) key.getPropertyValue(ARRAY_SHAPE);

                        try {
                            result = item.getData(origin, shape);
                            result = ((IArray) result).getArrayUtils().reduce().getArray();
                        } catch (InvalidRangeException e) {
                            LoggerFactory.getLogger(URISourceFactory.class.getName())
                                    .info("Unable to process command: " + key.getCommand() + "\n" + e.getMessage());
                        } catch (DataAccessException e) {
                            LoggerFactory.getLogger(URISourceFactory.class.getName())
                                    .info("Unable to process command: " + key.getCommand() + "\n" + e.getMessage());
                        }
                    }
                    break;
                }
                case Slice: {
                    if (node instanceof IDataItem) {
                        IDataItem item = (IDataItem) node;
                        int dim = (Integer) key.getPropertyValue(INT_DIM);
                        int pos = (Integer) key.getPropertyValue(INT_POS);
                        try {
                            result = item.getSlice(dim, pos);
                        } catch (InvalidRangeException e) {
                            LoggerFactory.getLogger(URISourceFactory.class.getName())
                                    .info("Unable to process command: " + key.getCommand() + "\n" + e.getMessage());
                        }
                    }
                    break;
                }
                case Shape: {
                    if (node instanceof IDataItem) {
                        result = ((IDataItem) node).getShape();
                    } else {
                        result = ArrayUtils.EMPTY_SHAPE;
                    }
                    break;
                }
                case Rank: {
                    result = 0;
                    if (node instanceof IDataItem) {
                        result = ((IDataItem) node).getRank();
                    }
                    break;
                }
                default:
                    result = null;
            }
        }

        return result;
    }

    /**
     * Convert the given data into a compatible data structure expected by the command
     * 
     * @param data
     * @param command
     * @return
     */
    @SuppressWarnings("unchecked")
    protected static Object convertData(final Object data, final CommandData command, final boolean unsigned) {
        Object result = data;
        if (data != null) {
            switch (command) {
                case DimensionValue:
                case Value:
                case Region:
                case Slice:
                case Attribute:
                    if (data instanceof IArray) {
                        IArray array = (IArray) data;
                        // if (array.getSize() > 1) {
                        AbstractMatrix<?> matrix = null;
                        if (array.getRank() <= 2) {
                            Object tab = array.getArrayUtils().copyTo1DJavaArray();
                            // Add unsigned information see JIRA EXPDATA-274
                            if (unsigned) {
                                tab = UnsignedConverter.convertUnsigned(tab);
                            }
                            Class<?> clazz = tab.getClass().getComponentType();

                            // Get the shape
                            int rank = array.getRank();
                            int y = 1;
                            if (rank > 0) {
                                y = array.getShape()[rank - 1];
                            }
                            int x = 1;
                            if (rank > 1) {
                                x = array.getShape()[rank - 2];
                            }

                            // Instantiate a corresponding matrix

                            if (Integer.TYPE.equals(clazz)) {
                                matrix = new IntMatrix();
                            } else if (Float.TYPE.equals(clazz)) {
                                matrix = new FloatMatrix();
                            } else if (Double.TYPE.equals(clazz)) {
                                matrix = new DoubleMatrix();
                            } else if (Short.TYPE.equals(clazz)) {
                                matrix = new ShortMatrix();
                            } else if (Long.TYPE.equals(clazz)) {
                                matrix = new LongMatrix();
                            } else if (Boolean.TYPE.equals(clazz)) {
                                matrix = new BooleanMatrix();
                            } else if (Byte.TYPE.equals(clazz)) {
                                matrix = new ByteMatrix();
                            } else if (String.class.equals(clazz)) {
                                matrix = new StringMatrix();
                            } else if (Character.class.equals(clazz) || Character.TYPE.equals(clazz)) {
                                matrix = new AbstractMatrix<Character>(Character.TYPE) {
                                    @Override
                                    protected Class<Character> generateDefaultDataType() {
                                        return Character.class;
                                    }

                                };
                            } else {
                                matrix = new BasicObjectMatrix((Class<Object>) array.getElementType());
                            }

                            if (matrix != null) {
                                try {
                                    matrix.setFlatValue(tab, x, y);
                                } catch (UnsupportedDataTypeException e) {
                                    Factory.getLogger().info("Unable to convert data");
                                    e.printStackTrace();
                                }
                            }
                        }
                        // Data having rank superior to 2 aren't managed
                        else {
                            matrix = null;
                        }
                        result = matrix;
                        // }
                        // else {
                        // result = array.getObject(array.getIndex());
                        // }
                    } else {
                        result = data;
                    }
                    break;
                default:
                    result = data;
                    break;
            }
        }
        return result;
    }

    // ---------------------------------------------------------
    // private methods
    // ---------------------------------------------------------
    /**
     * Return the expected data type for the given command
     * 
     * @param command
     * @note it aims to make possible the use of generics
     */
    private static Class<?> getDataType(final CDMAKey key, final IContainer node) {
        Class<?> clazz;
        Class<?> type = null;
        CommandData command = (CommandData) key.getCommand();
        switch (command) {
            case Attribute: {
                IAttribute attr = node.getAttribute((String) key.getPropertyValue(ATTRIBUTE_NAME));
                if (attr != null) {
                    type = attr.getType();
                }
            }
            case Region:
            case Slice:
            case Value:
                if (!command.equals(CommandData.Attribute)) {
                    type = ((IDataItem) node).getType();
                }
                clazz = null;
                if (type != null) {
                    clazz = ArrayUtils.newInstance(type, 0).getClass();
                }
                break;
            case Shape:
                clazz = int[].class;
                break;
            case Rank:
                clazz = Integer.class;
                break;
            case DimensionValue:
                clazz = null;
                if (node instanceof IGroup) {
                    String name = (String) key.getPropertyValue(DIMENSION_NAME);
                    IDimension dimension = ((IGroup) node).getDimension(name);
                    if (dimension != null) {
                        type = dimension.getCoordinateVariable().getElementType();
                        clazz = ArrayUtils.newInstance(type, 0).getClass();
                    }
                }
                break;
            default:
                clazz = null;
                break;
        }
        return clazz;
    }
}
