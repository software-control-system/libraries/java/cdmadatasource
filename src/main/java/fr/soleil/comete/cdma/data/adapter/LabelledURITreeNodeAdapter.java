/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.cdma.data.adapter;

import java.lang.ref.WeakReference;
import java.net.URI;
import java.util.Map;
import java.util.WeakHashMap;

import org.cdma.Factory;
import org.cdma.interfaces.IDatasource;
import org.cdma.utilities.LabelledURI;
import org.cdma.utilities.performance.PostTreatmentManager;

import fr.soleil.comete.cdma.data.internal.IconUpdater;
import fr.soleil.comete.definition.widget.util.BasicTreeNode;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.data.adapter.AbstractAdapter;
import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.lib.project.ObjectUtils;

public class LabelledURITreeNodeAdapter extends AbstractAdapter<LabelledURI, ITreeNode> {

    private static final Map<LabelledURI, Long> adaptedUrisTimestamps = new WeakHashMap<LabelledURI, Long>();
    private static final Map<LabelledURI, WeakReference<ITreeNode>> adaptedUris = new WeakHashMap<LabelledURI, WeakReference<ITreeNode>>();

    public LabelledURITreeNodeAdapter(Class<?> firstType, Class<?> secondType) {
        super(firstType, secondType);
    }

    @Override
    public ITreeNode adapt(LabelledURI data) throws DataAdaptationException {
        ITreeNode result = null;
        if (data != null) {
            synchronized (adaptedUris) {
                WeakReference<ITreeNode> ref = adaptedUris.get(data);
                if (ref != null) {
                    result = ref.get();
                }

                if (result == null) {
                    result = new BasicTreeNode();
                    URI uri = data.getURI();
                    result.setName(data.getLabel());
                    result.setData(uri);
                    result.setImage(IconUpdater.UNKNOWN_IMAGE);
                    // PostTreatmentManager.registerTreatment(new IconUpdater(data, result));
                    // Launch a parallel thread treatment !!!! WARNING !!!
                    // PostTreatmentManager.launchParallelTreatment(new IconUpdater(data, result));
                    PostTreatmentManager.registerTreatment(new IconUpdater(data, result));
                    adaptedUris.put(data, new WeakReference<ITreeNode>(result));
                    adaptedUrisTimestamps.put(data, data.getDatasource().getLastModificationDate(uri));
                } else {
                    // Don't register an IconUpdater thread for each LabelledURI in the tree
                    long uriTimeStamp = data.getDatasource().getLastModificationDate(data.getURI());
                    if (uriTimeStamp > adaptedUrisTimestamps.get(data)) {
                        // URI has changed -> we have to register a new IconUpdater thread
                        // Launch a parrallel thread treatment !!!! WARNING !!!
                        // PostTreatmentManager.launchParallelTreatment(new IconUpdater(data, result));
                        PostTreatmentManager.registerTreatment(new IconUpdater(data, result));
                        // PostTreatmentManager.registerTreatment(new IconUpdater(data, result));
                        adaptedUrisTimestamps.put(data, uriTimeStamp);
                    }
                }
            }
        }
        return result;
    }

    @Override
    public LabelledURI revertAdapt(ITreeNode data) throws DataAdaptationException {
        LabelledURI result = null;
        if (data != null) {
            if (data.getData() instanceof URI) {
                URI uri = (URI) data.getData();
                synchronized (adaptedUris) {
                    for (LabelledURI labelledURI : adaptedUris.keySet()) {
                        if (ObjectUtils.sameObject(labelledURI.getURI(), uri)) {
                            result = labelledURI;
                            break;
                        }
                    }
                }
                if (result == null) {
                    String label = data.getName();
                    IDatasource source = null;
                    for (IDatasource temp : Factory.getDatasources()) {
                        if (temp.isReadable(uri) || temp.isBrowsable(uri)) {
                            source = temp;
                            break;
                        }
                    }
                    result = new LabelledURI(label, uri, source);
                    synchronized (adaptedUris) {
                        adaptedUris.put(result, new WeakReference<ITreeNode>(data));
                    }
                }
            }
        }
        return result;
    }

}
