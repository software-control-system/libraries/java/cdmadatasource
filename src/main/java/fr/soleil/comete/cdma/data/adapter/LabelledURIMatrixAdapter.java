/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.comete.cdma.data.adapter;

import javax.activation.UnsupportedDataTypeException;

import org.cdma.utilities.LabelledURI;

import fr.soleil.comete.cdma.data.factories.URISourceFactory.LabelledURIMatrix;
import fr.soleil.data.adapter.AbstractAdapter;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.container.matrix.StringMatrix;
import fr.soleil.data.exception.DataAdaptationException;

public class LabelledURIMatrixAdapter extends AbstractAdapter<AbstractMatrix<LabelledURI>, AbstractMatrix<String>> {
    private final LabelledURIAdapter mAdapter;

    public LabelledURIMatrixAdapter(Class<?> firstType, Class<?> secondType) {
        super(firstType, secondType);
        mAdapter = new LabelledURIAdapter(LabelledURI.class, String.class);
    }

    @Override
    public AbstractMatrix<String> adapt(AbstractMatrix<LabelledURI> data) throws DataAdaptationException {
        StringMatrix result = null;

        if (data != null) {
            LabelledURI[] arrayUri = (LabelledURI[]) data.getFlatValue();
            String[] values = new String[arrayUri.length];

            for (int i = 0; i < values.length; i++) {
                values[i] = mAdapter.adapt(arrayUri[i]);
            }

            result = new StringMatrix();
            result.setFlatValue(values, data.getHeight(), data.getWidth());
        }

        return result;
    }

    @Override
    public AbstractMatrix<LabelledURI> revertAdapt(AbstractMatrix<String> data) throws DataAdaptationException {
        LabelledURIMatrix result = null;

        if (data != null) {
            String[] arrayUri = (String[]) data.getFlatValue();
            LabelledURI[] values = new LabelledURI[arrayUri.length];

            for (int i = 0; i < values.length; i++) {
                values[i] = mAdapter.revertAdapt(arrayUri[i]);
            }

            result = new LabelledURIMatrix();
            try {
                result.setFlatValue(values, data.getHeight(), data.getWidth());
            } catch (UnsupportedDataTypeException e) {
                throw new DataAdaptationException(e);
            }
        }

        return result;
    }

}
