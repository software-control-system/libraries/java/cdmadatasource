
/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
import java.io.File;
import java.net.URI;
import java.util.List;

import fr.soleil.comete.cdma.data.keys.CDMAKey;
import fr.soleil.comete.cdma.data.service.CDMADataSourceFactory;
import fr.soleil.comete.cdma.data.service.CDMAKeyFactory;
import fr.soleil.comete.cdma.data.service.CDMAKeyFactory.BrowsingMode;
import fr.soleil.comete.cdma.data.source.CDMADataSource;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.IDataSourceProducer;
import fr.soleil.data.source.AbstractDataSource;

public class CDMADataSourceTest {
    private static CDMAKeyFactory keyFactory = new CDMAKeyFactory();
    private static CDMAKeyFactory logicalFactory = new CDMAKeyFactory();
    private static IDataSourceProducer producer = null;

    public static void main(final String[] args) {
        String filePath = "src/test/resources/KatyScanImage.nxs";
        if ((args != null) && (args.length > 0)) {
            filePath = args[0];
        }

        DataSourceProducerProvider.pushNewProducer(CDMADataSourceFactory.class);
        producer = DataSourceProducerProvider.getProducerByClassName(CDMADataSourceFactory.class.getName());
        List<String> availablePlugins = CDMAKeyFactory.getAvailablePlugins();
        System.out.println("availablePlugins=" + availablePlugins);
        logicalFactory.enableMode(BrowsingMode.LOGICAL);

        openDataSetTwiceBug25135(filePath);
        // openDataSetPhysicalLogicalBug25160(filePath);
    }

    private static void openDataSetTwiceBug25135(final String filePath) {
        try {
            URI uri = new File(filePath).toURI();
            CDMAKey key = keyFactory.generateKeySourceTitle(uri);
            String infoKey = key.getInformationKey();

            System.out.println("####################First open####################");
            System.out.println("Dataset " + infoKey + " isOpened=" + CDMAKeyFactory.isOpenedDataset(uri));
            System.out.println("Open dataset " + infoKey);
            System.out.println("producer=" + producer);
            AbstractDataSource<?> createDataSource = producer.createDataSource(key);
            traceSource(createDataSource);
            System.out.println("Dataset " + infoKey + " isOpened=" + CDMAKeyFactory.isOpenedDataset(uri));
            System.out.println("Close dataset " + infoKey);
            CDMAKeyFactory.unregisterDataset(key);
            ((CDMADataSourceFactory) producer).removeDataSource(key);
            System.out.println("Dataset " + infoKey + " isOpened=" + CDMAKeyFactory.isOpenedDataset(uri));

            System.out.println("####################Second open####################");
            System.out.println("Open dataset " + infoKey);
            createDataSource = producer.createDataSource(key);
            traceSource(createDataSource);
            System.out.println("Dataset " + infoKey + " isOpened=" + CDMAKeyFactory.isOpenedDataset(uri));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void traceSource(final AbstractDataSource<?> createDataSource) {
        if (createDataSource instanceof CDMADataSource<?>) {
            try {
                ((CDMADataSource<?>) createDataSource).updateData();
                Object data = createDataSource.getData();
                if (data != null) {
                    System.out.println("Value = " + data.toString());
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

//    private static void openDataSetPhysicalLogicalBug25160(final String filePath) {
//        try {
//            URI uri = new File(filePath).toURI();
//            CDMAKey key = keyFactory.generateKeySourceTitle(uri);
//            String infoKey = key.getInformationKey();
//
//            URI logicalURI = new URI(filePath + "#%2Fa");
//            CDMAKey logicalKey = logicalFactory.generateKeySourceTitle(logicalURI);
//            String logicalInfoKey = logicalKey.getInformationKey();
//
//            System.out.println("####################Logical open####################");
//            System.out.println("Dataset " + infoKey + " isOpened=" + CDMAKeyFactory.isOpenedDataset(uri));
//            System.out.println("Open dataset " + logicalInfoKey);
//            System.out.println("Value = " + producer.createDataSource(logicalKey).getData().toString());
//            System.out.println("Dataset " + infoKey + " isOpened=" + CDMAKeyFactory.isOpenedDataset(uri));
//
//            System.out.println("####################Physical open####################");
//            System.out.println("Open dataset " + infoKey);
//            System.out.println("Value = " + producer.createDataSource(key).getData().toString());
//            System.out.println("Dataset " + infoKey + " isOpened=" + CDMAKeyFactory.isOpenedDataset(uri));
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
