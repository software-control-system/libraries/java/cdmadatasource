/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package panels;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.net.URI;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.border.TitledBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.cdma.Factory;

import fr.soleil.comete.box.complexbox.TreeViewerBox;
import fr.soleil.comete.cdma.data.keys.CDMAKey;
import fr.soleil.comete.cdma.data.service.CDMAKeyFactory;
import fr.soleil.comete.definition.widget.util.BasicTreeNode;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.comete.swing.TextField;
import fr.soleil.comete.swing.Tree;
import fr.soleil.comete.swing.util.CometeTreeModel;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.application.performance.Benchmarker;
import fr.soleil.lib.project.swing.WindowSwingUtils;
import fr.soleil.lib.project.swing.dialog.ProgressDialog;

public class UriTreePanel {

    private static final String WAITING = "Scanning URI '%s'...";
    private static final String ALONE = "alone";

    private final Manager manager;
    private final TextField sourceTextField;
    private final TreeViewerBox treeBox;
    private final Tree browseUriTree;
    private final ActionListener sourceListener;
    private final JPanel uriTreePanel;
    private final JPanel mainPanel;
    private ProgressDialog waitingDialog;

    public UriTreePanel(Manager manager) {
        this.manager = manager;
        sourceListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getWaitingDialog().setMainMessage(String.format(WAITING, sourceTextField.getText()));
                getWaitingDialog().pack();
                getWaitingDialog().setLocationRelativeTo(getPanel());
                getWaitingDialog().setVisible(true);
                SwingWorker<Void, Void> uriWorker = new SwingWorker<Void, Void>() {
                    @Override
                    protected Void doInBackground() throws Exception {
                        refreshAll(sourceTextField.getText());
                        return null;
                    }

                    @Override
                    protected void done() {
                        getWaitingDialog().setVisible(false);
                    }
                };
                uriWorker.execute();
            }
        };
        sourceTextField = new TextField();
        sourceTextField.setTitledBorder("Entry point");
        sourceTextField.setText(manager.getURI().toString());
        sourceTextField.addActionListener(sourceListener);
        browseUriTree = new BrowseTree();
        treeBox = new TreeViewerBox();
        treeBox.setUseChildren(browseUriTree, true);
        treeBox.setUseSelectionToWrite(browseUriTree, true);
        mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(sourceTextField, BorderLayout.NORTH);
        uriTreePanel = new JPanel(new GridLayout(1, 1));
        JScrollPane treeScrollPane = new JScrollPane(browseUriTree);
        treeScrollPane.setBorder(new TitledBorder("Browse URIs"));
        uriTreePanel.add(treeScrollPane);
        mainPanel.add(uriTreePanel, BorderLayout.CENTER);
        waitingDialog = null;
        refreshAll(sourceTextField.getText());
    }

    public ProgressDialog getWaitingDialog() {
        if (waitingDialog == null) {
            waitingDialog = new ProgressDialog(WindowSwingUtils.getWindowForComponent(getPanel()));
            waitingDialog.setTitle(String.format(WAITING, ObjectUtils.EMPTY_STRING));
            waitingDialog.setMainMessage(waitingDialog.getTitle());
            waitingDialog.setProgressIndeterminate(true);
        }
        return waitingDialog;
    }

    public JPanel getPanel() {
        return mainPanel;
    }

    protected void refreshAll(String text) {
        if ((text != null) && (!text.trim().isEmpty())) {
            Benchmarker.setLogger(Factory.getLogger());

            String uriText = text.replace(File.separator, "/");
            sourceTextField.setText(text);
            treeBox.disconnectWidgetFromAll(browseUriTree);
            URI uri = URI.create(uriText);
            ITreeNode rootNode = new BasicTreeNode();
            rootNode.setData(uri);
            rootNode.setName(text);
            browseUriTree.setRootNode(rootNode);
            browseUriTree.setSelectionRow(0);
            rootNode = new BasicTreeNode();
            rootNode.setData(uri);
            rootNode.setName(text);
            CDMAKeyFactory factory = manager.getKeyFactory();
            CDMAKey key = factory.generateKeyListValidURI(uri);

            // use a standalone key to avoid interactions with UriPanel
            key.registerProperty(ALONE, CometeUtils.generateIdForClass(CDMAKey.class));
            treeBox.connectWidget(browseUriTree, key, false, false);
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    browseUriTree.expandAll(true);
                }
            });

            String bench = Benchmarker.print();
            if (!bench.isEmpty()) {
                System.out.println(bench);
                Benchmarker.reset();
            }
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    private abstract class DoubleClickTree extends Tree {

        private static final long serialVersionUID = 1298894020319322505L;

        public DoubleClickTree() {
            this(null);
        }

        public DoubleClickTree(CometeTreeModel model) {
            super(model);
            addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent e) {
                    mouseChanged(e);
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                    mouseChanged(e);
                }

                @Override
                public void mouseClicked(MouseEvent e) {
                    mouseChanged(e);
                }
            });
        }

        private void mouseChanged(MouseEvent e) {
            if ((e != null) && (e.getSource() == this) && (e.getClickCount() == 2)) {
                e.consume();
                treatDoubleSelection();
            }
        }

        protected abstract void treatDoubleSelection();

        protected void updateManagerURI() {
            if (manager != null) {
                TreePath path = getSelectionPath();
                if (path != null) {
                    DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
                    if ((node != null) && (node.isLeaf())) {
                        ITreeNode treeNode = getModel().getTreeNode(node);
                        if ((treeNode != null) && (treeNode.getData() instanceof URI)) {
                            manager.setURI((URI) treeNode.getData());
                        }
                    }
                }
            }
        }
    }

    private class BrowseTree extends DoubleClickTree {

        private static final long serialVersionUID = -8104873872125805767L;

        public BrowseTree() {
            super();
        }

        @Override
        protected void treatDoubleSelection() {
            ITreeNode[] selectedNodes = getSelectedNodes();
            if ((selectedNodes != null) && (selectedNodes.length > 0) && (selectedNodes[0].getChildren().isEmpty())) {
                targetDelegate.warnMediators(new TargetInformation<ITreeNode>(this, selectedNodes[0]));
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        expandPath(getSelectionPath());
                        updateManagerURI();
                    }
                });
            }
        }

    }
}
