/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package panels;

import java.awt.BorderLayout;
import java.net.URI;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import fr.soleil.comete.cdma.data.service.CDMADataSourceFactory;
import fr.soleil.comete.cdma.data.service.CDMAKeyFactory;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.lib.project.ObjectUtils;

public class Manager extends JPanel {

    private static final long serialVersionUID = 1365988133123587958L;

    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;

    private URI fileName;
    private CDMAKeyFactory factory;
    private String plugin;
    private String[] path = new String[] {};

    public Manager(String fileName, String view) {
        this.factory = (view == null || view.trim().isEmpty() ? new CDMAKeyFactory() : new CDMAKeyFactory(view));
        this.fileName = URI.create(fileName);

        layoutComponents();
        plugin = null;
    }

    public URI getURI() {
        return fileName;
    }

    public void setURI(URI uri) {
        fileName = uri;
    }

    public CDMAKeyFactory getKeyFactory() {
        return factory;
    }

    public void setKeyFactory(CDMAKeyFactory factory) {
        this.factory = factory;
    }

    public CDMADataSourceFactory getSourceFactory() {
        return (CDMADataSourceFactory) DataSourceProducerProvider.getProducer(CDMADataSourceFactory.SOURCE_PRODUCER_ID);
    }

    public String[] getPath() {
        return this.path;
    }

    public void setPath(String[] path) {
        this.path = path;
    }

    private void layoutComponents() {
        this.setLayout(new BorderLayout());
        JTabbedPane tabbedPane = new JTabbedPane();

        // URI panel
        UriPanel uriPanel = new UriPanel(this);
        tabbedPane.add("URI", uriPanel.getPanel());

        // URI tree panel
        UriTreePanel uriTreePanel = new UriTreePanel(this);
        tabbedPane.add("URI Tree", uriTreePanel.getPanel());

        // Browser panel
        BrowserPanel browPanel = new BrowserPanel(this);
        tabbedPane.addTab("Browser", browPanel.getPanel());

        // Scalar panel
        ScalarPanel scalPanel = new ScalarPanel(this);
        tabbedPane.addTab("Scalar", scalPanel.getPanel());

        // Chart panel
        SpectrumPanel specPanel = new SpectrumPanel(this);
        tabbedPane.addTab("Chart", specPanel.getPanel());

        // Image panel
        ImagePanel imagePanel = new ImagePanel(this);
        tabbedPane.addTab("Image", imagePanel.getPanel());

        tabbedPane.setVisible(true);
        add(tabbedPane, BorderLayout.CENTER);

    }

    public static String stringsToPath(String[] path) {
        String result = ObjectUtils.EMPTY_STRING;
        for (String node : path) {
            if (!node.isEmpty()) {
                if (!result.isEmpty()) {
                    result += ",";
                }
                result += node;
            }
        }

        return result;
    }

    public void setPlugin(String name) {
        plugin = name;
    }

    public String getPlugin() {
        return plugin;
    }
}
