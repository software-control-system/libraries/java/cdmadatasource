/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package panels;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URI;
import java.util.EventObject;

import javax.swing.JPanel;

import org.cdma.Factory;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.comete.box.matrixbox.StringMatrixBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.cdma.data.factories.ArraySourceFactory;
import fr.soleil.comete.cdma.data.keys.CDMAKey;
import fr.soleil.comete.cdma.data.service.CDMAKeyFactory;
import fr.soleil.comete.definition.listener.IComboBoxListener;
import fr.soleil.comete.definition.widget.IComboBox;
import fr.soleil.comete.swing.StringMatrixComboBoxViewer;
import fr.soleil.comete.swing.TextArea;
import fr.soleil.comete.swing.TextField;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.lib.project.application.performance.Benchmarker;

public class ScalarPanel {
    private final Manager manager;

    private TextArea scalarValue;
    private TextField pathTextField;
    private StringMatrixComboBoxViewer groupListCombo;
    private StringMatrixComboBoxViewer itemsListCombo;

    // Listener
    private IComboBoxListener groupComboListener;
    private IComboBoxListener itemsComboListener;

    // Connector
    private final StringScalarBox stringScalarBox;
    private final StringMatrixBox stringMatrixBox;

    public ScalarPanel(Manager manager) {
        this.manager = manager;
        stringMatrixBox = new StringMatrixBox();
        stringScalarBox = new StringScalarBox();
        init();
    }

    public JPanel getPanel() {
        // Scalar panel
        JPanel scalarPanel = new JPanel();
        scalarPanel.setLayout(new GridLayout(4, 1));
        scalarPanel.add(scalarValue);
        scalarPanel.add(pathTextField);
        scalarPanel.add(groupListCombo);
        scalarPanel.add(itemsListCombo);
        return scalarPanel;
    }

    private void init() {
        scalarValue = new TextArea();
        scalarValue.setTitledBorder("Select a data to set its data");
        pathTextField = new TextField();
        pathTextField.setTitledBorder("Current path");

        groupListCombo = new StringMatrixComboBoxViewer();
        groupListCombo.setTitledBorder("Available groups");
        itemsListCombo = new StringMatrixComboBoxViewer();
        itemsListCombo.setTitledBorder("Available items");

        // Validate on current path
        pathTextField.addActionListener(updateFields());

        // Select a node
        itemsComboListener = changeItem();
        groupComboListener = changeGroup();
        groupListCombo.addComboBoxListener(groupComboListener);
        itemsListCombo.addComboBoxListener(itemsComboListener);
    }

    // ------------------------------------------------------------------------
    // Update SI
    // ------------------------------------------------------------------------
    private void changeNode(final IComboBox component) {
        // Update current path text field
        String path = Manager.stringsToPath(manager.getPath());
        if (!path.isEmpty()) {
            path += ",";
        }
        path += component.getText();
        pathTextField.setText(path);
    }

    private synchronized void refreshAll() {
        Benchmarker.setLogger(Factory.getLogger());

        groupListCombo.removeComboBoxListener(groupComboListener);
        itemsListCombo.removeComboBoxListener(itemsComboListener);

        stringMatrixBox.disconnectWidgetFromAll(groupListCombo);
        stringMatrixBox.disconnectWidgetFromAll(itemsListCombo);

        // Get the current URI from manager
        URI uri = manager.getURI();

        // Update the manager
        String[] path = pathTextField.getText().split(",");
        manager.setPath(path);

        CDMAKeyFactory factory = manager.getKeyFactory();
        CDMAKey key;

        // Connect combo group
        key = factory.generateKeyGroupList(uri, path);
        stringMatrixBox.connectWidget(groupListCombo, key, false, true);

        // Connect combo item
        key = factory.generateKeyItemList(uri, path);
        stringMatrixBox.connectWidget(itemsListCombo, key, false, true);

        groupListCombo.addComboBoxListener(groupComboListener);
        itemsListCombo.addComboBoxListener(itemsComboListener);

        String bench = Benchmarker.print();
        if (!bench.isEmpty()) {
            System.out.println(bench);
            Benchmarker.reset();
        }
    }

    // ------------------------------------------------------------------------
    // Listeners
    // ------------------------------------------------------------------------
    private IComboBoxListener changeGroup() {
        groupComboListener = new IComboBoxListener() {

            @Override
            public void selectedItemChanged(EventObject event) {
                changeNode(groupListCombo);
                refreshAll();
            }
        };
        return groupComboListener;
    }

    private IComboBoxListener changeItem() {
        itemsComboListener = new IComboBoxListener() {

            @Override
            public void selectedItemChanged(EventObject event) {
                changeNode(itemsListCombo);
                refreshAll();

                CDMAKeyFactory factory = manager.getKeyFactory();
                CDMAKey key = factory.generateKeyArrayRank(manager.getURI(), manager.getPath());
                // CDMADataSourceFactory sourceFactory = new CDMADataSourceFactory();
                // AbstractDataSource<?> src = sourceFactory.createDataSource(key);

                try {
                    // int rank = (Integer) src.getData();
                    // if (rank == 0) {
                    final AbstractCometeBox<ITextTarget> scalarBox = stringScalarBox;
                    key = factory.generateKeyArrayValue(manager.getURI(), manager.getPath());
                    scalarBox.connectWidget(scalarValue, key, false, true);
                    // }
                } catch (Exception e) {
                    e.printStackTrace();
                    LoggerFactory.getLogger(ArraySourceFactory.class.getName())
                            .info("Unable to get rank\n" + e.getMessage());
                }
            }
        };
        return itemsComboListener;
    }

    private ActionListener updateFields() {
        ActionListener result = new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                refreshAll();
            }
        };
        return result;
    }
}
