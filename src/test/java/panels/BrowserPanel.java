/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package panels;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URI;
import java.util.EventObject;

import javax.swing.JPanel;

import org.cdma.Factory;

import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.comete.box.matrixbox.StringMatrixBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.cdma.data.keys.CDMAKey;
import fr.soleil.comete.cdma.data.service.CDMAKeyFactory;
import fr.soleil.comete.definition.listener.IComboBoxListener;
import fr.soleil.comete.definition.widget.IComboBox;
import fr.soleil.comete.swing.StringMatrixComboBoxViewer;
import fr.soleil.comete.swing.TextArea;
import fr.soleil.comete.swing.TextField;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.application.performance.Benchmarker;

public class BrowserPanel {
    private final Manager manager;

    // Graphic component
    private TextField pathTextField;
    private StringMatrixComboBoxViewer groupListCombo;
    private StringMatrixComboBoxViewer itemsListCombo;
    private StringMatrixComboBoxViewer attrListCombo;
    private TextArea descriptionTextArea;
    private TextArea datasetTextArea;

    // Listener
    private IComboBoxListener groupComboListener;
    private IComboBoxListener itemsComboListener;
    private ActionListener attrActionListener;

    // Connector
    private final StringScalarBox stringScalarBox;
    private final StringMatrixBox stringMatrixBox;

    public BrowserPanel(Manager manager) {
        this.manager = manager;
        stringScalarBox = new StringScalarBox();
        stringMatrixBox = new StringMatrixBox();
        init();
    }

    public JPanel getPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(6, 1));
        panel.add(datasetTextArea);
        panel.add(pathTextField);
        panel.add(groupListCombo);
        panel.add(itemsListCombo);
        panel.add(attrListCombo);
        panel.add(descriptionTextArea);

        return panel;
    }

    private void init() {
        datasetTextArea = new TextArea();
        datasetTextArea.setTitledBorder("Source name");

        descriptionTextArea = new TextArea();
        descriptionTextArea.setTitledBorder("Attribute value");

        pathTextField = new TextField();
        pathTextField.setTitledBorder("Current path");
        pathTextField.setText(Manager.stringsToPath(manager.getPath()));

        groupListCombo = new StringMatrixComboBoxViewer();
        groupListCombo.setTitledBorder("Available groups");

        itemsListCombo = new StringMatrixComboBoxViewer();
        itemsListCombo.setTitledBorder("Available items");

        attrListCombo = new StringMatrixComboBoxViewer();
        attrListCombo.setTitledBorder("Available attributes");

        // Validate on current path
        pathTextField.addActionListener(updateFields());

        // Select a node
        groupListCombo.addComboBoxListener(changeGroup());
        itemsListCombo.addComboBoxListener(changeItem());
        attrListCombo.addActionListener(changeAttribute());
    }

    private ActionListener changeAttribute() {
        attrActionListener = new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (attrListCombo.isValid()) {
                    final AbstractCometeBox<ITextTarget> scalarBox = stringScalarBox;
                    URI uri = manager.getURI();
                    String attr = attrListCombo.getText();
                    if (attr != null) {
                        String[] path = manager.getPath();
                        CDMAKeyFactory factory = manager.getKeyFactory();
                        CDMAKey key = factory.generateKeyAttributeValue(uri, path, attr);
                        scalarBox.connectWidget(descriptionTextArea, key, false, true);
                    }
                }
            }
        };
        return attrActionListener;
    }

    private IComboBoxListener changeGroup() {
        groupComboListener = new IComboBoxListener() {

            @Override
            public void selectedItemChanged(EventObject event) {
                changeNode(groupListCombo);
                refreshAll();
            }
        };
        return groupComboListener;
    }

    private IComboBoxListener changeItem() {
        itemsComboListener = new IComboBoxListener() {

            @Override
            public void selectedItemChanged(EventObject event) {
                changeNode(itemsListCombo);
                refreshAll();
            }
        };
        return itemsComboListener;
    }

    private ActionListener updateFields() {
        ActionListener result = new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                refreshAll();
            }
        };
        return result;
    }

    private void changeNode(final IComboBox component) {
        // // Get the current URI from manager
        // URI uri = manager.getURI();

        // Update current path text field
        String path = Manager.stringsToPath(manager.getPath());
        if (!path.isEmpty()) {
            path += ",";
        }
        path += component.getText();
        pathTextField.setText(path);
    }

    private synchronized void refreshAll() {
        Benchmarker.setLogger(Factory.getLogger());

        groupListCombo.removeComboBoxListener(groupComboListener);
        itemsListCombo.removeComboBoxListener(itemsComboListener);
        attrListCombo.removeActionListener(attrActionListener);

        stringMatrixBox.disconnectWidgetFromAll(groupListCombo);
        stringMatrixBox.disconnectWidgetFromAll(itemsListCombo);
        stringMatrixBox.disconnectWidgetFromAll(attrListCombo);
        stringScalarBox.disconnectWidgetFromAll(datasetTextArea);

        // Get the current URI from manager
        URI uri = manager.getURI();

        // Update the manager
        String[] path = pathTextField.getText().split(",");
        manager.setPath(path);

        CDMAKeyFactory factory = manager.getKeyFactory();
        CDMAKey key;

        // Connect combo group
        key = factory.generateKeyGroupList(uri, path);
        stringMatrixBox.connectWidget(groupListCombo, key, false, true);

        // Connect combo item
        key = factory.generateKeyItemList(uri, path);
        stringMatrixBox.connectWidget(itemsListCombo, key, false, true);

        // Connect sourceBrowsable
        key = factory.generateKeySourceTitle(uri);
        stringScalarBox.connectWidget(datasetTextArea, key, false, true);

        // Connect combo attribute
        key = factory.generateKeyAttributeList(uri, path);
        stringMatrixBox.connectWidget(attrListCombo, key, false, true);

        descriptionTextArea.setText(ObjectUtils.EMPTY_STRING);

        groupListCombo.addComboBoxListener(groupComboListener);
        itemsListCombo.addComboBoxListener(itemsComboListener);
        attrListCombo.addActionListener(attrActionListener);

        String bench = Benchmarker.print();
        if (!bench.isEmpty()) {
            System.out.println(bench);
            Benchmarker.reset();
        }
    }
}
