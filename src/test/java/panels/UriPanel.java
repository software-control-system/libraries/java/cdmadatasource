/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package panels;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URI;
import java.util.EventObject;

import javax.swing.JPanel;

import org.cdma.Factory;

import fr.soleil.comete.box.matrixbox.StringMatrixBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.cdma.data.keys.CDMAKey;
import fr.soleil.comete.cdma.data.service.CDMADataSourceFactory;
import fr.soleil.comete.cdma.data.service.CDMAKeyFactory;
import fr.soleil.comete.definition.listener.IComboBoxListener;
import fr.soleil.comete.swing.StringMatrixComboBoxViewer;
import fr.soleil.comete.swing.TextArea;
import fr.soleil.comete.swing.TextField;
import fr.soleil.data.service.PolledRefreshingStrategy;
import fr.soleil.lib.project.application.performance.Benchmarker;

public class UriPanel {
    private final Manager manager;

    private TextField sourceParent;
    private TextArea sourceBrowsable;
    private TextArea sourceReadable;
    private TextArea sourceExperiment;
    private TextField sourceTextField;
    private StringMatrixComboBoxViewer availablePlugins;
    private StringMatrixComboBoxViewer sourceComboBox;

    private final StringMatrixBox stringMatrixBox;
    private final StringScalarBox stringScalarBox;
    private IComboBoxListener sourceComboListener;
    private IComboBoxListener pluginsComboListener;

    public UriPanel(Manager manager) {
        this.manager = manager;
        stringScalarBox = new StringScalarBox();
        stringMatrixBox = new StringMatrixBox();
        init();
        stringMatrixBox.setCleanWidgetOnDisconnect(sourceComboBox, false);
        stringMatrixBox.setCleanWidgetOnDisconnect(availablePlugins, false);
        stringScalarBox.setCleanWidgetOnDisconnect(sourceReadable, false);
        stringScalarBox.setCleanWidgetOnDisconnect(sourceBrowsable, false);
        stringScalarBox.setCleanWidgetOnDisconnect(sourceExperiment, false);
        stringScalarBox.setCleanWidgetOnDisconnect(sourceParent, false);
    }

    public JPanel getPanel() {
        JPanel uriPanel = new JPanel();
        uriPanel.setLayout(new GridLayout(7, 1));
        uriPanel.add(availablePlugins);
        uriPanel.add(sourceParent);
        uriPanel.add(sourceTextField);
        uriPanel.add(sourceComboBox);
        uriPanel.add(sourceBrowsable);
        uriPanel.add(sourceReadable);
        uriPanel.add(sourceExperiment);
        refreshAll(sourceTextField.getText());
        return uriPanel;
    }

    private void init() {
        sourceBrowsable = new TextArea();
        sourceReadable = new TextArea();
        sourceExperiment = new TextArea();
        sourceParent = new TextField();
        sourceBrowsable.setTitledBorder("is browsable");
        sourceReadable.setTitledBorder("is readable");
        sourceExperiment.setTitledBorder("is experiment");
        sourceParent.setTitledBorder("Parent URI");

        availablePlugins = new StringMatrixComboBoxViewer();
        availablePlugins.setTitledBorder("Available plug-ins");

        sourceTextField = new TextField();
        sourceTextField.setTitledBorder("Currently scanned URI");
        sourceTextField.setText(manager.getURI().toString());
        sourceTextField.addActionListener(updateFields(sourceTextField));
        sourceParent.addActionListener(updateFields(sourceParent));

        sourceComboBox = new StringMatrixComboBoxViewer();
        sourceComboBox.setTitledBorder("Available URIs");

        // Update text field when combo is changed
        sourceComboBox.addComboBoxListener(updateTextFieldWithComboSelection(sourceComboBox));
        availablePlugins.addComboBoxListener(updateActivePluginWithCombo(availablePlugins));
    }

    private ActionListener updateFields(final TextField field) {
        ActionListener result = new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                refreshAll(field.getText());
            }
        };
        return result;
    }

    private IComboBoxListener updateActivePluginWithCombo(final StringMatrixComboBoxViewer sourceComboBox) {
        pluginsComboListener = new IComboBoxListener() {
            @Override
            public void selectedItemChanged(EventObject event) {
                if (sourceComboBox.isValid()) {
                    String text = sourceComboBox.getText();
                    setPlugin(text);
                }
            }
        };
        return pluginsComboListener;
    }

    private IComboBoxListener updateTextFieldWithComboSelection(final StringMatrixComboBoxViewer sourceComboBox) {
        sourceComboListener = new IComboBoxListener() {
            @Override
            public void selectedItemChanged(EventObject event) {
                if (sourceComboBox.isValid()) {
                    String text = sourceComboBox.getText();
                    refreshAll(text);
                }
            }
        };
        return sourceComboListener;
    }

    private synchronized void setPlugin(String text) {
        manager.setPlugin(text);
        URI uri = manager.getURI();
        refreshAll(uri.toString());
    }

    private synchronized void refreshAll(String text) {
        if ((text != null) && (!text.trim().isEmpty())) {
            Benchmarker.setLogger(Factory.getLogger());

            sourceTextField.setText(text);
            sourceComboBox.removeComboBoxListener(sourceComboListener);
            availablePlugins.removeComboBoxListener(pluginsComboListener);

            stringMatrixBox.disconnectWidgetFromAll(sourceComboBox);
            stringScalarBox.disconnectWidgetFromAll(sourceReadable);
            stringScalarBox.disconnectWidgetFromAll(sourceBrowsable);
            stringScalarBox.disconnectWidgetFromAll(sourceExperiment);
            stringScalarBox.disconnectWidgetFromAll(sourceParent);
            stringScalarBox.disconnectWidgetFromAll(availablePlugins);

            // String text_loc = sourceTextField.getText();
            URI uri = URI.create(text);

            // Update the manager
            manager.setURI(uri);
            manager.setPath(new String[0]);

            CDMADataSourceFactory refresher = manager.getSourceFactory();
            CDMAKeyFactory factory = manager.getKeyFactory();
            CDMAKey key;
            String activePlugin = manager.getPlugin();

            // Connect combo
            key = factory.generateKeyListValidURI(uri);
            key.setPlugin(activePlugin);
            stringMatrixBox.connectWidget(sourceComboBox, key, false, true);
            if (refresher != null) {
                refresher.setRefreshingStrategy(key, new PolledRefreshingStrategy(1000));
            }

            // Connect available plug-ins
            key = factory.generateKeyListAvailablePlugins();
            stringMatrixBox.connectWidget(availablePlugins, key, false, true);

            // Connect sourceBrowsable
            key = factory.generateKeyIsReadableURI(uri);
            // key.setPlugin(activePlugin);
            stringScalarBox.connectWidget(sourceReadable, key, false, true);

            // Connect sourceReadable
            key = factory.generateKeyIsBrowsableURI(uri);
            // key.setPlugin(activePlugin);
            stringScalarBox.connectWidget(sourceBrowsable, key, false, true);

            // Connect sourceExperiment
            key = factory.generateKeyIsExperimentURI(uri);
            // key.setPlugin(activePlugin);
            stringScalarBox.connectWidget(sourceExperiment, key, false, true);

            // Connect sourceParent
            key = factory.generateKeyGetParentURI(uri);
            // key.setPlugin(activePlugin);
            stringScalarBox.connectWidget(sourceParent, key, false, true);

            sourceComboBox.addComboBoxListener(sourceComboListener);
            availablePlugins.addComboBoxListener(pluginsComboListener);

            String bench = Benchmarker.print();
            if (!bench.isEmpty()) {
                System.out.println(bench);
                Benchmarker.reset();
            }
        }
    }
}
