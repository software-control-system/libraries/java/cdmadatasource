
/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
import java.awt.Dimension;
import java.io.File;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import org.cdma.Factory;

import fr.soleil.comete.cdma.data.service.CDMAKeyFactory;
import fr.soleil.lib.project.ObjectUtils;
import panels.Manager;

public class MyFirstTest {
    private static String sample = ObjectUtils.EMPTY_STRING;
    private static String dictionaries = ObjectUtils.EMPTY_STRING;

    private static String view = ObjectUtils.EMPTY_STRING;

    public static void main(String[] args) throws Exception, InvocationTargetException {
        for (String arg : args) {
            String[] values = arg.split("=");
            if (values.length > 1) {
                String param = values[0];
                String value = values[1];

                if (param.equals("DICTIONARIES")) {
                    dictionaries = value.replace(File.separator, "/");
                } else if (param.equals("SAMPLES")) {
                    sample = value.replace(File.separator, "/");
                } else if (param.equals("VIEW")) {
                    view = value;
                }
            }
        }

        Factory.setDictionariesFolder(dictionaries);

        CDMAKeyFactory f = new CDMAKeyFactory();
        System.out.println("available views:");
        for (String string : f.getAvailableViews()) {
            System.out.println("  - :" + string);
        }

        SwingUtilities.invokeAndWait(new Runnable() {
            @Override
            public void run() {
                JFrame mainFrame = new JFrame("CDMA TESTER");
                mainFrame.setSize(new Dimension(800, 600));
                mainFrame.setVisible(true);

                Manager tester;
                tester = new Manager(sample, view);
                mainFrame.setContentPane(tester);
                mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            }
        });

    }

}
